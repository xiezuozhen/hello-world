package com.xzz.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;


/**
 * @ClassName RabbitMQConfig
 * @Description
 * @Author xzz
 * @Date 2024/10/12 11:28
 */
@Configuration
public class RabbitMQConfig {
    //定义队列名
    private static final String QUEUE = "queue";


    //创建队列
    /**
     * 1. 配置队列
     * 2. 队列名为 queue
     * 3. true 表示: 持久化 (不填，默认为true,默认持久化)
     * durable： 队列是否持久化。 队列默认是存放到内存中的，rabbitmq 重启则丢失，
     * 若想重启之后还存在则队列要持久化，
     * 保存到 Erlang 自带的 Mnesia 数据库中，当 rabbitmq 重启之后会读取该数据库
     * @return
     */
    @Bean
    public Queue queue(){
        return new Queue(QUEUE,true);
    }

    private static final String QUEUE1 = "queue_fanout01";
    private static final String QUEUE2 = "queue_fanout02";
    private static final String EXCHANGE = "fanoutExchange";



    //--------fanout广播模式---------
    /**
     * 1. 配置队列
     * 2. 队列名为 queue
     * 3. true 表示: 持久化 (不填，默认为true,默认持久化)
     * durable： 队列是否持久化。 队列默认是存放到内存中的，rabbitmq 重启则丢失，
     * 若想重启之后还存在则队列要持久化，
     * 保存到 Erlang 自带的 Mnesia 数据库中，当 rabbitmq 重启之后会读取该数据库
     * @return
     */
    @Bean
    public Queue queue1(){
        return new Queue(QUEUE1);
    }

    @Bean
    public Queue queue2(){
        return new Queue(QUEUE2);
    }

    //创建交换机
    @Bean
    public FanoutExchange exchange(){
        return new FanoutExchange(EXCHANGE);
    }

    //将队列和交换机进行绑定
    @Bean
    public Binding binding01(){
        //将队列queue1和交换机进行绑定
        return BindingBuilder.bind(queue1()).to(exchange());
    }

    @Bean
    public Binding binding02(){
        //将队列queue1和交换机进行绑定
        return BindingBuilder.bind(queue2()).to(exchange());
    }

    //direct
    private static final String QUEUE_DIRECT1 = "queue_direct01";
    private static final String QUEUE_DIRECT2 = "queue_direct02";
    private static final String EXCHANGE_DIRECT = "directExchange";
    //路由
    private static final String routing_key01 = "queue.red";
    private static final String routing_key02 = "queue.green";

    //--------direct路由模式---------
    @Bean
    public Queue queue_direct1() {
        return new Queue(QUEUE_DIRECT1);
    }

    @Bean
    public Queue queue_direct2() {
        return new Queue(QUEUE_DIRECT2);
    }

    @Bean
    public DirectExchange exchange_direct() {
        return new DirectExchange(EXCHANGE_DIRECT);
    }

    @Bean
    public Binding binding_direct1() {
        //将队列queue_direct1和交换机进行绑定，并给队列绑定路由
        return BindingBuilder.bind(queue_direct1()).to(exchange_direct()).with(routing_key01);
    }

    @Bean
    public Binding binding_direct2() {
        //将队列queue_direct2和交换机进行绑定，并给队列绑定路由
        return BindingBuilder.bind(queue_direct2()).to(exchange_direct()).with(routing_key02);
    }


    //topic主题模式
    private static final String QUEUE_TOPIC1 = "queue_topic01";
    private static final String QUEUE_TOPIC2 = "queue_topic02";
    private static final String EXCHANGE_TOPIC = "topicExchange";
    //路由
    private static final String topic_routing_key01 = "#.queue.#";
    private static final String topic_routing_key02 = "*.queue.#";

    @Bean
    public Queue queue_topic01() {
        return new Queue(QUEUE_TOPIC1);
    }

    @Bean
    public Queue queue_topic02() {
        return new Queue(QUEUE_TOPIC2);
    }

    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange(EXCHANGE_TOPIC);
    }

    @Bean
    public Binding binding_topic1() {
        //将队列queue1和交换机进行绑定
        return BindingBuilder.bind(queue_topic01()).to(topicExchange()).with(topic_routing_key01);
    }

    @Bean
    public Binding binding_topic2() {
        //将队列queue1和交换机进行绑定
        return BindingBuilder.bind(queue_topic02()).to(topicExchange()).with(topic_routing_key02);
    }


    private static final String QUEUE01 = "queue_header01";
    private static final String QUEUE02 = "queue_header02";
    private static final String HEADER_EXCHANGE = "headersExchange";

    @Bean
    public Queue queue_header01() {
        return new Queue(QUEUE01);
    }

    @Bean
    public Queue queue_header02() {
        return new Queue(QUEUE02);
    }

    @Bean
    public HeadersExchange headersExchange() {
        return new HeadersExchange(HEADER_EXCHANGE);
    }

    @Bean
    public Binding binding_header01() {
        Map<String,Object> map = new HashMap<>();
        map.put("color","red");
        map.put("speed","low");
        System.out.println("yy=" + headersExchange().hashCode());
        //whereAny(map): 只要发送的消息的属性 MessageProperties 有任意一个k-v匹配就 OK
        return BindingBuilder.bind(queue_header01()).to(headersExchange()).whereAny(map).match();
    }

    @Bean
    public Binding binding_header02() {
        Map<String, Object> map = new HashMap<>();
        map.put("color", "red");
        map.put("speed", "fast");
        System.out.println("xx=" + headersExchange().hashCode());
        //whereAll(map): 发送的消息的属性 MessageProperties 要全部匹配才 OK
        return BindingBuilder.bind(queue_header02()).to(headersExchange()).whereAll(map).match();
    }


}
