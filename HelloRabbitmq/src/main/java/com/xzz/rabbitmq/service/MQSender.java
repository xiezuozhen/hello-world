package com.xzz.rabbitmq.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @ClassName MQSender
 * @Description
 * @Author xzz
 * @Date 2024/10/12 11:31
 */
@Slf4j
@Service
public class MQSender {
    @Resource
    private RabbitTemplate rabbitTemplate;

    //方法：发送消息
    public void send(Object msg){
        log.info("发送消息-" + msg);
        //没有指定交换机会走默认的交换机，AMQP default
        //AMQP default是一个direct路由模式的交换机
        rabbitTemplate.convertAndSend("queue",msg);
    }

    //fanout广播模式发送消息
    public void sendFanout(Object msg){
        log.info("发送消息-" + msg);
        //因为是fanout广播模式，不需要指定路由，这里路由赋空值处理
        rabbitTemplate.convertAndSend("fanoutExchange","",msg);
    }


    public void sendDirect1(Object msg){
        log.info("发送消息-" + msg);
        rabbitTemplate.convertAndSend("directExchange","queue.red",msg);
    }

    public void sendDirect2(Object msg){
        log.info("发送消息-" + msg);
        rabbitTemplate.convertAndSend("directExchange","queue.green",msg);
    }

    public void sendTopic1(Object msg) {
        log.info("发送消息-" + msg);
        rabbitTemplate.convertAndSend("topicExchange", "queue.red.message", msg);
    }

    public void sendTopic2(Object msg) {
        log.info("发送消息-" + msg);
        rabbitTemplate.convertAndSend("topicExchange", "green.queue.green.message", msg);
    }


    public void sendHeader1(String msg) {
        MessageProperties properties = new MessageProperties();
        properties.setHeader("color", "red");
        properties.setHeader("speed", "fast");
        Message message = new Message(msg.getBytes(), properties);
        rabbitTemplate.convertAndSend("headersExchange","",message);
    }

    public void sendHeader2(String msg) {
        MessageProperties properties = new MessageProperties();
        properties.setHeader("color", "red");
        properties.setHeader("speed", "normal");
        Message message = new Message(msg.getBytes(), properties);
        rabbitTemplate.convertAndSend("headersExchange","",message);
    }
}
