package com.xzz.rabbitmq.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * @ClassName MQReceiver
 * @Description
 * @Author xzz
 * @Date 2024/10/12 11:32
 */
@Service
@Slf4j
public class MQReceiver {
    //方法：接收消息
    @RabbitListener(queues = "queue")
    public void receive(Object msg) {
        log.info("接收到消息--" + msg);
    }

    //queues对应接收消息的队列
    @RabbitListener	(queues = "queue_fanout01")
    public void receive1(Object msg) {
        log.info("从 queue_fanout01 接收消息-" + msg);
    }

    @RabbitListener(queues = "queue_fanout02")
    public void receive2(Object msg){
        log.info("从 queue_fanout02 接收消息-" + msg);
    }

    @RabbitListener(queues = "queue_direct01")
    public void queue_direct1(Object msg){
        log.info("从 queue_direct1 接收消息-" + msg);
    }

    @RabbitListener(queues = "queue_direct02")
    public void queue_direct2(Object msg){
        log.info("从 queue_direct2 接收消息-" + msg);
    }

    @RabbitListener(queues = "queue_topic01")
    public void queue_topic1(Object msg) {
        log.info("从 queue_topic01 接收消息-" + msg);
    }

    @RabbitListener(queues = "queue_topic02")
    public void queue_topic2(Object msg) {
        log.info("从 queue_topic02 接收消息-" + msg);
    }

    @RabbitListener(queues = "queue_header01")
    public void queue_header1(Message message) {
        log.info("queue_header01 接收消息 message 对象" + message);
        log.info("queue_header01 接收消息" + new String(message.getBody()));
    }

    @RabbitListener(queues = "queue_header02")
    public void queue_header2(Message message) {
        log.info("queue_header2 接收消息 message 对象" + message);
        log.info("queue_header2 接收消息" + new String(message.getBody()));
    }

}
