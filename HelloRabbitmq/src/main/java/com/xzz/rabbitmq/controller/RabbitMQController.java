package com.xzz.rabbitmq.controller;

import com.xzz.rabbitmq.service.MQSender;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ClassName RabbitMQController
 * @Description
 * @Author xzz
 * @Date 2024/10/12 11:33
 */
@RestController
public class RabbitMQController {
    //装配MQSender
    @Resource
    private MQSender mqSender;

    //方法：调用消息生产者，发送消息
    @RequestMapping("/mq")
    public void mq(){
        mqSender.send("hello llp");
    }

    //调用消息生产者，发送消息到交换机
    @RequestMapping("/mq/fanout")
    public void fanout(){
        mqSender.sendFanout("hello fanout~");
    }

    //direct 模式
    @GetMapping("/mq/direct01")
    public void direct01() {
        mqSender.sendDirect1("hello aimee");
    }

    //direct 模式
    @GetMapping("/mq/direct02")
    public void direct02() {
        mqSender.sendDirect2("hello llp");
    }

    //topic 模式
    @GetMapping("/mq/topic01")
    public void topic01() {
        mqSender.sendTopic1("hello aimee topic");
    }

    //topic 模式
    @GetMapping("/mq/topic02")
    public void topic02() {
        mqSender.sendTopic2("hello llp topic");
    }


    //header 模式
    @GetMapping("/mq/header01")
    public void header01() {
        mqSender.sendHeader1("hello llp header1");
    }

    //header 模式
    @GetMapping("/mq/header02")
    public void header02() {
        mqSender.sendHeader2("hello llp header2");
    }


    //延迟消息模式
    @GetMapping("/mq/delay01")
    public void delay01() {
        mqSender.sendHeader2("hello llp header2");
    }


}
