package com.xzz.rabbitmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName HelloRabbitmqApplication
 * @Description
 * @Author xzz
 * @Date 2024/10/12 11:24
 */
@Slf4j
@SpringBootApplication
public class HelloRabbitmqApplication {
    public static void main(String[] args) {
        SpringApplication.run(HelloRabbitmqApplication.class, args);
    }
}
