package com.xzz.activiti.initdb;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.ProcessEngines;

/**
 * 初始化activiti数据表
 */
@Slf4j
public class InitTables {


    public static void main(String[] args) {

        init1();
    }

    /**
     * 方式1
     */
    public static void init1() {

        ProcessEngineConfiguration configuration
                = ProcessEngineConfiguration.createStandaloneProcessEngineConfiguration();
        configuration.setDatabaseSchemaUpdate("true");
        configuration.setJdbcDriver("com.mysql.cj.jdbc.Driver");
        configuration.setJdbcUrl("jdbc:mysql://localhost:3306/test?serverTimezone=PRC&nullCatalogMeansCurrent=true&useUnicode=true&charaterEncoding=utf-8&useSSL=false");
        configuration.setJdbcUsername("root");
        configuration.setJdbcPassword("as7718718?");
        ProcessEngine processEngine = configuration.buildProcessEngine();
//        log.info("成功获取到引擎={}，初始化数据表完成",processEngine);
    }

    /**
     * 方式2，用这种方式必须保证resource目录下存在activiti.cfg.xml文件，
     * 否则defaultProcessEngine为null并且不能初始化出25张数据表
     */
    public static void init2() {

        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
//        log.info("成功获取到引擎={}，初始化数据表完成",defaultProcessEngine);
    }

    /**
     * 方式3，用这种方式必须保证resource目录下存在activiti.cfg.xml文件，
     * 否则会抛出异常并且不能初始化出25张数据表
     */
    public static void init3() {

        ProcessEngine processEngine = ProcessEngineConfiguration.createProcessEngineConfigurationFromResourceDefault().buildProcessEngine();
//        log.info("成功获取到引擎={}，初始化数据表完成",processEngine);
    }

}
