package com.xzz.activiti.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName MyActivitiController
 * @Description
 * @Author xzz
 * @Date 2024/9/20 9:30
 */
@Controller
@Slf4j
@RequestMapping("/activiti-explorer/service")
public class MyActivitiController {
    @Resource
    private RepositoryService repositoryService;

    @Resource
    private ProcessEngine processEngine;

    /**
     * @Description 创建模型
     * @author weixinxin
     * @Date 18:12 2023/6/30
     **/
    @RequestMapping(value = "/create")
    public void create(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = "";
        String name = "请假申请-Travel approval process";
        String description = "请假申请-Travel approval process";
        String key = "leave_approval_process";
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode editorNode = objectMapper.createObjectNode();
        editorNode.put("id", "canvas");
        editorNode.put("resourceId", "canvas");
        ObjectNode stencilSetNode = objectMapper.createObjectNode();
        stencilSetNode.put("namespace", "http://b3mn.org/stencilset/bpmn2.0#");
        editorNode.put("stencilset", stencilSetNode);
        Model modelData = this.repositoryService.newModel();
        ObjectNode modelObjectNode = objectMapper.createObjectNode();
        modelObjectNode.put("name", name);
        modelObjectNode.put("revision", 1);
        description = StringUtils.defaultString(description);
        modelObjectNode.put("description", description);
        modelData.setMetaInfo(modelObjectNode.toString());
        modelData.setName(name);
        modelData.setKey(StringUtils.defaultString(key));
        this.repositoryService.saveModel(modelData);
        this.repositoryService.addModelEditorSource(modelData.getId(), editorNode.toString().getBytes("utf-8"));
        id = modelData.getId();

        //一定要输出id，部署需要，刚接触activiti不知道这个id在哪里
        System.out.println("id ========= " + id);
        response.sendRedirect("/modeler.html?modelId=" + id);
    }

    /**
     * @param id
     * @Description 发布模型为流程定义
     * @Date 14:04 2023/07/06
     * @author weixinxin
     */
    @RequestMapping("/deploy/{id}")
    @ResponseBody
    public void deploy(@PathVariable("id") String id) throws Exception {
        Model modelData = repositoryService.getModel(id);
        byte[] bytes = repositoryService.getModelEditorSource(modelData.getId());
        JsonNode modelNode = new ObjectMapper().readTree(bytes);
        BpmnModel model = new BpmnJsonConverter().convertToBpmnModel(modelNode);
        if (model.getProcesses().size() == 0) {
//            log.error("数据模型不符要求，请至少设计一条主线流程。");
            System.out.println("数据模型不符要求，请至少设计一条主线流程。");
        }
        byte[] bpmnBytes = new BpmnXMLConverter().convertToXML(model);
        String processName = modelData.getName() + ".bpmn20.xml";
        repositoryService.createDeployment()
                .name(modelData.getName())
                .addString(processName, new String(bpmnBytes, "UTF-8"))
                .deploy();
    }
    /**
     * @param keyName
     * @Description 启动流程
     * @Date 14:03 2023/07/06
     * @author weixinxin
     */
    @RequestMapping("/start/{key}")
    @ResponseBody
    public Object startProcess(@PathVariable("key") String keyName) {
        // 业务Key
        String businessKey = "666";
        // 变量：注意实际情况下候选人Assignee的值一般是用户的id而不是账号或者姓名。
        Map<String, Object> variables = new HashMap<>();
        variables.put("creator", "zhangsan");
        variables.put("pm", "狗经理");
        variables.put("hr", "小姐姐");
        ProcessInstance process = processEngine.getRuntimeService().startProcessInstanceByKey(keyName, businessKey, variables);
        System.out.println(process.getId());
        System.out.println(process.getProcessInstanceId());
        System.out.println(process.getProcessDefinitionId());
        return process.getId() + " : " + process.getProcessDefinitionId();
    }

    /**
     * @param processInstanceId 流程实例id
     * @Description 提交任务
     * @Date 14:03 2023/07/06
     * @author weixinxin
     */
    @RequestMapping("/run/{processInstanceId}")
    @ResponseBody
    public Object run(@PathVariable("processInstanceId") String processInstanceId) {
        Task task = processEngine.getTaskService().createTaskQuery().processInstanceId(processInstanceId).singleResult();
        System.out.println("task {} find " + task.getId());
        processEngine.getTaskService().complete(task.getId());
        return "SUCCESS";
    }

//    @RequestMapping("/delete/{id}")
//    public Object deleteModel(@PathVariable("id")String id){
//        repositoryService.deleteModel(id);
//        return "删除成功。";
//    }
//
//    @GetMapping("/model/{id}/json")
//    public Object modelJson(@PathVariable("id")String id){
//        // 获取模型
//        Model model = repositoryService.getModel(id);
//        // 获取JSON字符串
//        String json = new String(repositoryService.getModelEditorSource(model.getId()), StandardCharsets.UTF_8);
//        return json;
//    }

}
