package com.xzz.activiti.test;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.Test;

public class HelloWorldTest {
    /**
     * 启动流程实例：发起申请请求
     */
    @Test
    public void startProcessInstance() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        ProcessInstance processInstance = processEngine.getRuntimeService()
                .startProcessInstanceByKey("1");
        System.out.println(processInstance.getId());
        System.out.println(processInstance.getProcessInstanceId());
        System.out.println(processInstance.getProcessDefinitionId());
    }
}
