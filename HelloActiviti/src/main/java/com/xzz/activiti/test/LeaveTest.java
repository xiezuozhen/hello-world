package com.xzz.activiti.test;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class LeaveTest {
    /**
     * 启动流程实例：发起申请请求
     */
    @Test
    public void startProcessInstance() {
        // 业务Key
        String businessKey = "666";
        // 变量：注意实际情况下候选人Assignee的值一般是用户的id而不是账号或者姓名。
        Map<String, Object> variables = new HashMap<>();
        variables.put("creator", "zhangsan");
        variables.put("pm", "狗经理");
        variables.put("hr", "小姐姐");
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        ProcessInstance processInstance = processEngine.getRuntimeService()
                .startProcessInstanceByKey("leave", businessKey, variables);
        System.out.println(processInstance.getId());
        System.out.println(processInstance.getProcessInstanceId());
        System.out.println(processInstance.getProcessDefinitionId());
    }

}
