package com.xzz.activiti;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName HelloActivitiApplication
 * @Description
 * @Author xzz
 * @Date 2024/9/19 14:12
 */
@Slf4j
@SpringBootApplication(exclude = {org.activiti.spring.boot.SecurityAutoConfiguration.class,
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class })
public class HelloActivitiApplication {
    public static void main(String[] args) {
        SpringApplication.run(HelloActivitiApplication.class, args);
    }
}
