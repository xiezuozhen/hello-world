package com.xzz.redis.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisService {
	@Autowired
	StringRedisTemplate redisTemplate;
	@Test
	public void redisStringTest(){
		ValueOperations<String, String> opsForValue = redisTemplate.opsForValue();
		opsForValue.set("msg1","你好哇！");
		System.out.println(opsForValue.get("msg1"));
	}
}
