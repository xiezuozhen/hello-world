package com.xzz.redis.limit.impl;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class AccessLimiterImpl{

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 计数器限流算法(无法应对突发大流量)
     * @param key  限制接口的标识
     * @param times 访问次数
     * @param per 一段时间
     * @param unit 时间单位
     * @return
     */
    public boolean isCountLimited(String key, Long times, Long per, TimeUnit unit) {
        // 根据专门储存访问次数的key 将里面的数值加1
        Long curTimes = redisTemplate.boundValueOps(key).increment(1);
        log.info("curTimes: {}",curTimes);
        if (curTimes>times){
            log.error("超频访问:[{}]",key);
            return true;
        }else {
            if (curTimes == 1){
                log.info(" set expire");
                redisTemplate.boundValueOps(key).expire(per,unit);

            }
            return false;
        }
    }
    /**
     * 滑动窗口算法
     * https://www.yht7.com/news/178167
     * @param key
     * @param maxRequest 最大请求数
     * @param period:窗口大小
     * @return
     */
    public boolean isSlidingWindowLimited(String key,int maxRequest,int period) {
    	long nowMSecons = System.currentTimeMillis();
    	//添加一个元素到有序列表中
    	redisTemplate.opsForZSet().add(key, UUID.randomUUID().toString(), System.currentTimeMillis());
    	redisTemplate.opsForZSet().removeRangeByScore(key, 0, nowMSecons - period * 1000);
    	long count = redisTemplate.opsForZSet().zCard(key);
    	redisTemplate.opsForZSet().score(key, period+1);
    	
    	
    	
    	return false;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
}
