package com.xzz.test.config;

import java.util.Date;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.xzz.test.entity.User;

//标记一个spring配置类
//相当于一个xml<beans>标签中的内容
//https://www.cnblogs.com/0099-ymsml/p/16328543.html
@Configuration
//@Import(MyConfig.class)
@ComponentScan(basePackages = "com.xzz.test")
public class IocJavaConfig {
	@Bean
	public User getUser() {
		return new User();
	}
}
