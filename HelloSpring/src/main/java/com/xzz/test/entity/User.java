package com.xzz.test.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;
@Component
public class User implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2922441905281463811L;
	
	private Integer age = 3;
    private Character sex = 'm';
    private Date bir = new Date();
    private String name = "xzz";
    public User() {
    }
    //此处省略get set 构造器。。。
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Character getSex() {
		return sex;
	}
	public void setSex(Character sex) {
		this.sex = sex;
	}
	public Date getBir() {
		return bir;
	}
	public void setBir(Date bir) {
		this.bir = bir;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public User(Integer age, Character sex, Date bir, String name) {
		super();
		this.age = age;
		this.sex = sex;
		this.bir = bir;
		this.name = name;
	}
    
}

