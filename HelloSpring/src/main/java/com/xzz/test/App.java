package com.xzz.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.xzz.test.config.IocJavaConfig;
import com.xzz.test.entity.User;

public class App {
	public static void main(String[] args) {
		//定位、载入和解析注册
//		ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext("springcontext.xml");
//      通过Spring创建User对象     Spring配置文件中对象的ID
		//依赖注入
//		User user = (User) app.getBean("user");
//		System.out.println(user);
		
//		AopC
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(IocJavaConfig.class);
        User user = applicationContext.getBean("user", User.class);
        System.out.println(user.getName());
		
		
	}
}
