package com.xzz.test.spitest;

import java.util.ServiceLoader;

import com.xzz.test.spi.UploadCDN;

public class SPITest {
	public static void main(String[] args) {
		ServiceLoader<UploadCDN> uploadCDN = ServiceLoader.load(UploadCDN.class);
		for (UploadCDN u : uploadCDN) {
	        u.upload("filePath");
	    }
	}
}
