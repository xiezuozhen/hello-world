package com.xzz.zookeeper.test;

import java.util.List;

import org.I0Itec.zkclient.ZkClient;
import org.apache.zookeeper.CreateMode;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.xzz.zookeeper.ZookeeperApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ZookeeperApplication.class)
public class SpringBootZkClientApplicationTests {

    @Autowired
    public ZkClient zkClient;

    /**
     * 创建节点
     *
     * @throws Exception
     */
    @Test
//    @Ignore
    public void createNode() throws Exception {
        // 添加持久节点
        String path = zkClient.create("/zkclient-node", "xiezz", CreateMode.PERSISTENT);
        System.out.println("created path:" + path);
    }

    /**
     * 获取子节点
     *
     * @throws Exception
     */
    @Test
    @Ignore
    public void testGetChildrenData() throws Exception {
    	List<String> cList = zkClient.getChildren("/zkclient-node");
    	System.out.println(cList.toString());
    }

    

    

    /**
     * 删除节点
     *
     * @throws Exception
     */
    @Test
//    @Ignore
    public void testDelete() throws Exception {
        boolean e1 = zkClient.delete("/zkclient-node");
        boolean e2 = zkClient.deleteRecursive("/zkclient-node");
        System.out.println(e1);
        System.out.println(e2);
    }
}

