package com.xzz.zookeeper.configure.zkclient;

import org.I0Itec.zkclient.ZkClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ZookeeperConfig {
	@Autowired
    WrapperZKClient wrapperZk;

    @Bean
    public ZkClient zkClient() {
//        ZkClient zkClient = new ZkClient(wrapperZk.getConnectString(), wrapperZk.getSessionTimeoutMs(), wrapperZk.getConnectionTimeoutMs());
        ZkClient zkClient = new ZkClient(wrapperZk.getConnectString());
        return zkClient;
    }


}

