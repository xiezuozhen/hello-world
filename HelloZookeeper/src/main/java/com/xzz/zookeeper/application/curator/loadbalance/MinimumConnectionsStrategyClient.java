package com.xzz.zookeeper.application.curator.loadbalance;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.curator.framework.CuratorFramework;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 最小连接数策略 Demo
 * Client 客户端发送请求
 */
@Component
public class MinimumConnectionsStrategyClient{
    // Curator 客户端
    @Autowired
    public CuratorFramework client;
    // 服务列表节点的 父节点
    public static final String IMOOC_SERVER = "/5axxw-server";


    /**
     * 获取最小连接数的服务
     * 发送请求前调用此方法，获取服务地址
     *
     * @return String
     * @throws Exception Exception
     */
    public String getTheMinimumNumberOfConnectionsService() throws Exception {
        // 获取所有子节点
        List<String> list = client.getChildren().forPath(IMOOC_SERVER);
        // 新建 Map
        Map<String, Integer> map = new HashMap<>();
        // 遍历服务列表，保存服务地址与请求会话数的映射关系
        for (String s : list) {
            byte[] bytes = client.getData().forPath(IMOOC_SERVER + "/" + s);
            int i = Integer.parseInt(new String(bytes));
            map.put(s, i);
        }
        // 寻找 map 中会话数最小的值
        Optional<Map.Entry<String, Integer>> min = map.entrySet().stream().min(Map.Entry.comparingByValue());
        // 不为空的话
        if (min.isPresent()) {
            // 返回 服务地址 ip
            Map.Entry<String, Integer> entry = min.get();
            return entry.getKey();
        } else {
            // 没有则返回服务列表第一个服务地址 ip
            return list.get(0);
        }
    }

    /**
     * 增加该服务的请求会话数量
     * 使用服务地址处理业务前调用此方法
     *
     * @param ip 服务地址
     * @throws Exception Exception
     */
    public void increaseTheNumberOfRequestedSessions(String ip) throws Exception {
        byte[] bytes = client.getData().forPath(IMOOC_SERVER + "/" + ip);
        int i = Integer.parseInt(new String(bytes));
        i++;
        client.setData().forPath(IMOOC_SERVER + "/" + ip, String.valueOf(i).getBytes());
    }

    /**
     * 减少该服务的请求会话数量
     * 请求结束时调用此方法减少会话数量
     *
     * @param ip 服务地址
     * @throws Exception Exception
     */
    public void reduceTheNumberOfRequestedSessions(String ip) throws Exception {
        byte[] bytes = client.getData().forPath(IMOOC_SERVER + "/" + ip);
        int i = Integer.parseInt(new String(bytes));
        i--;
        client.setData().forPath(IMOOC_SERVER + "/" + ip, String.valueOf(i).getBytes());
    }
}
