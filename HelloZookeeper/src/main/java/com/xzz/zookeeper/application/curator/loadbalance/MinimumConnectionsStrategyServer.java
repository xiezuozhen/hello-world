package com.xzz.zookeeper.application.curator.loadbalance;

import java.net.InetAddress;

import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 * 最小连接数策略 Demo
 * Server 服务端注册地址
 */
@Component
public class MinimumConnectionsStrategyServer {
    // Curator 客户端
    @Autowired
    public CuratorFramework client;
    // 当前服务地址的临时节点
    public static String SERVER_IP;
    // 当前服务地址临时节点的父节点，节点类型为持久节点
    public static final String IMOOC_SERVER = "/5axxw-server";

    
    /**
     * 注册地址信息到 Zookeeper
     * 服务启动时和服务手动上线时调用此方法
     *
     * @throws Exception Exception
     */
    public void registerAddressToZookeeper() throws Exception {
        // 判断父节点是否存在，不存在则创建持久节点
        Stat stat = client.checkExists().forPath(IMOOC_SERVER);
        if (stat == null) {
            client.create().creatingParentsIfNeeded().forPath(IMOOC_SERVER);
        }
        // 获取本机地址
        String address = InetAddress.getLocalHost().getHostAddress();
        // 创建临时节点，节点路径为 /IMOOC_SERVER/address，节点 data 为 请求会话数，初始化时为 0.
        // /5axxw-server/192.168.0.77
        SERVER_IP = client.create()
                .withMode(CreateMode.EPHEMERAL)
                .forPath(IMOOC_SERVER + "/" + address, "0".getBytes());
    }

    /**
     * 注销在 Zookeeper 上的注册的地址
     * 服务手动下线时调用此方法
     *
     * @throws Exception Exception
     */
    public void deregistrationAddress() throws Exception {
        // 检查该节点是否存在
        Stat stat = client.checkExists().forPath(SERVER_IP);
        // 存在则删除
        if (stat != null) {
            client.delete().forPath(SERVER_IP);
        }
    }
}
