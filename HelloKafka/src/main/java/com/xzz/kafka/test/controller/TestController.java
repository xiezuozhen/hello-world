package com.xzz.kafka.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xzz.kafka.test.service.KafkaProducerService;


@RestController
@RequestMapping("/v1")
public class TestController {
	@Autowired
	private KafkaProducerService kafkaProducerService;
	/**
	 * 单条信息的发送
	 * @throws Exception
	 */
	@GetMapping("/firstMessage")
	public void testSendMessageSync() throws Exception {
      String topic = "hello-kafka-test-topic";
      String key = "key1";
      String message = "第1条消息";
      kafkaProducerService.sendMessageSync(topic, key, message);
	}
	/**
	 * 多条信息的发送
	 * @throws Exception
	 */
	@GetMapping("/multiMessage")
	public void testMultiSendMessageSync() throws Exception {
      String topic = "hello-kafka-test-topic";
      for (int i = 0; i < 10; i++) {
    	  String key = "key"+i;
    	  String message = "第"+i+"条消息";
          kafkaProducerService.sendMessageSync(topic, key, message);
      }
	}
	/**
	 * 多条信息的发送(自己分区)
	 * @throws Exception
	 */
	@GetMapping("/multiMessageInPartition")
	public void testMultiSendMessageSyncInPartition() throws Exception {
      String topic = "hello-kafka-test-topic";
      for (int i = 0; i < 10; i++) {
    	  String key = "key"+i;
    	  String message = "第"+i+"条消息";
    	  int partition = i % 3;
          kafkaProducerService.sendMessageSyncInPartition(topic, key, message, partition);
      }
	}
	/**
	 * 根据key进行分区
	 * @throws Exception
	 */
	@GetMapping("/multiMessageByKey")
	public void testMultiSendMessageSyncByKey() throws Exception {
      String topic = "hello-kafka-test-topic";
      for (int i = 0; i < 10; i++) {
    	  String key = "key"+i;
    	  String message = "第"+i+"条消息";
          kafkaProducerService.sendMessageSyncByKey(topic, key, message);
      }
	}
	
	/**
	 * 结果回调
	 * @throws Exception
	 */
	@GetMapping("/helloSendMessageGetResult")
	public void testSendMessageGetResult() throws Exception {
      String topic = "hello-kafka-test-topic";
      String key = "key";
      String message = "helloSendMessageGetResult";
      kafkaProducerService.sendMessageGetResult(topic, key, message);
      kafkaProducerService.sendMessageGetResult(topic, null, message);
	}
	
	/**
	 * 异步消息发送
	 */
	@GetMapping("/firstAsyncMessage")
	public void testSendMessageAsync() {
		String topic = "hello-kafka-test-topic";
		String message = "firstAsyncMessage";
		kafkaProducerService.sendMessageAsync(topic, message);
	}
	/**
	 * 创建消息的构建器
	 * @throws Exception
	 */
	@GetMapping("/helloMessageBuilder")
	public void testMessageBuilder() throws Exception {
		String topic = "hello-kafka-test-topic";
		String key = "key1";
		String message = "helloMessageBuilder";
		kafkaProducerService.testMessageBuilder(topic, key, message);
	}
	
	/**
	 * 发送事务消息
	 */
	@GetMapping("/helloSendMessageInTransaction")
	public void testSendMessageInTransaction() {
		String topic = "hello-kafka-test-topic";
		String key = "key1";
		String message = "helloSendMessageInTransaction";
		kafkaProducerService.sendMessageInTransaction(topic, key, message);
	}
	
	/**
	 * 批量发送消息
	 * @throws Exception
	 */
	@GetMapping("/testConsumerBatch")
	public void testConsumerBatch() throws Exception {
		// 写入多条数据到批量topic：hello-batch
		String topic = "hello-batch";
		for (int i = 0; i < 20; i++) {
			kafkaProducerService.sendMessageSync(topic, null, "batchMessage" + i);
		}
	}
	/**
	 * 消费者拦截器
	 * @throws Exception
	 */
	@GetMapping("/testConsumerInterceptor")
	public void testConsumerInterceptor() throws Exception {
		String topic = "consumer-interceptor";
		for (int i = 0; i < 2; i++) {
			kafkaProducerService.sendMessageSync(topic, null, "normalMessage" + i);
		}
		kafkaProducerService.sendMessageSync(topic, null, "filteredMessage");
		kafkaProducerService.sendMessageSync(topic, null, "filterMessage");
	}
	/**
	 * kafka实现分布式事务
	 * @throws Exception
	 */
	@GetMapping("/testConsumerInterceptor")
	public void testDistrbuteTransaction() throws Exception {
		//需要配合数据库
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
