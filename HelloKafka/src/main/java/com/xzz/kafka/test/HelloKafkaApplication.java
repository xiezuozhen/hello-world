package com.xzz.kafka.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
/**
 * https://blog.csdn.net/qq_39340792/article/details/117534578
 * @author Administrator
 *
 */
@SpringBootApplication
@EnableKafka
public class HelloKafkaApplication {
    public static void main(String[] args) {
        SpringApplication.run(HelloKafkaApplication.class, args);
    }
}