package com.xzz.mongodb.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCount {
	//姓名
    private String name;
    //年龄
    private Integer age;
    //等级
    private Integer grade;
    //总数
    private int totalUser;
    //平均分
    private double avgUser;
}
