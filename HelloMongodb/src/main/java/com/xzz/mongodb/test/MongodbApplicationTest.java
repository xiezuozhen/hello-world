package com.xzz.mongodb.test;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.bson.Document;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.mapreduce.MapReduceOptions;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;

import com.mongodb.MapReduceOutput;
import com.mongodb.client.result.UpdateResult;
import com.xzz.mongodb.entity.MapReduceResult;
import com.xzz.mongodb.entity.User;
import com.xzz.mongodb.entity.UserCount;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MongodbApplicationTest {
	@Autowired
    private MongoTemplate mongoTemplate;
	
	/**
	 * 集合查询
	 */
	@Ignore
	@Test
    public void testMongoDB() {
        List<User> userList = mongoTemplate.findAll(User.class);
        userList.forEach(item->{
            System.out.println(item);
        });
    }
	
	/**
	 * 批量插入
	 */
	@Ignore
	@Test
	public void saveBatchMongoDB() {
        List<User> userList = new ArrayList<User>();
        userList.add(new User("谢作震",29,3));
        userList.add(new User("张思",29,3));
        mongoTemplate.insert(userList, "user");
    }
	
	
	/**
	 * 单条插入
	 */
	@Ignore
	@Test
	public void saveSingleMongoDB() {
		User user = new User("谢理智",31,5);
		mongoTemplate.insert(user);
	}
	
	/**
	 * 精确匹配
	 */
	@Ignore
	@Test
	public void findMongoDB() {
		Query query = new Query(Criteria.where("name").is("张三"));
		User findOne = mongoTemplate.findOne(query,User.class,"user");
		System.out.println(findOne);
	}
	
	/**
	 * 模糊匹配
	 */
	@Ignore
	@Test
	public void fuzzyMongoDB() {
		Pattern pattern = Pattern.compile("张",Pattern.CASE_INSENSITIVE);
		Query query = new Query(Criteria.where("name").regex(pattern));
		List<User> userList = mongoTemplate.find(query,User.class,"user");
		userList.forEach(item->{
            System.out.println(item);
        });
	}
	
	/**
	 * 多条件查询
	 */
	@Ignore
	@Test
	public void findList() {
		Criteria c = new Criteria();
		c.and("name").is("张三");
		c.and("age").is("12");
		Query query = Query.query(c);
		List<User> findList = mongoTemplate.find(query, User.class,"user");
		findList.forEach(item->{
            System.out.println(item);
        });
	}
	
	/**
	 * 分页查询
	 */
	@Ignore
	@Test
	public void pageMongoDB() {
		Criteria c = new Criteria();
		c.and("grade").is(3);
		Query query = Query.query(c);
		// 设置分页
		query.limit(3).skip(1);
		List<User> findList = mongoTemplate.find(query, User.class,"user");
		findList.forEach(item->{
            System.out.println(item);
        });
	}
	
	/**
	 * 排序
	 */
	@Ignore
	@Test
	public void sortMongoDB() {
		Query query = new Query(Criteria.where("grade").is(3));
		query.with(new Sort(Sort.Direction.ASC,"name"));
//		query.with(Sort.by(Sort.Order.asc("name")));
		List<User> findList = mongoTemplate.find(query,User.class,"user");
		findList.forEach(item->{
            System.out.println(item);
        });
	}
	
	/**
	 * 计数
	 */
	@Ignore
	@Test
	public void countMongoDB() {
		Query query = new Query(Criteria.where("grade").is(3));
		long count = mongoTemplate.count(query,User.class,"user");
		System.out.println(count);
	}
	
	/**
	 * 更新
	 */
	@Ignore
	@Test
	public void updateMongoDB() {
		Query query = new Query(); 
		query.addCriteria(Criteria.where("name").is("张三"));  //_id区分引号 "1"和1
		Update update = Update.update("age", 12);
//		WriteResult upsert = mongoTemplate.updateMulti(query, update, "userList"); //查询到的全部更新
//		WriteResult upsert = mongoTemplate.updateFirst(query, update, "userList"); //查询更新第一条
		UpdateResult upsert = mongoTemplate.upsert(query, update, "user");      //有则更新，没有则新增
		System.out.println(upsert.getMatchedCount());       //返回执行的条数
	}
	
	/**
	 * 添加内嵌文档(即数组)
	 */
	@Ignore
	@Test
	public void addNestingMongoDB() {
		Query query = Query.query(Criteria.where("_id").is(1));
		User user = new User("沙雕", 7, 21);
		Update update = new Update();
		update.addToSet("user", user);
		UpdateResult upsert = mongoTemplate.upsert(query, update, "user");
		System.out.println(upsert.getMatchedCount());       //返回执行的条数
	}
	
	/**
	 * 修改内嵌文档
	 */
	@Ignore
	@Test
	public void updateNestingMongoDB() {
		Query query = Query.query(Criteria.where("_id").is("1").and("user.name").is("小明"));
		Update update = Update.update("user.$.age", 1);
		UpdateResult upsert = mongoTemplate.upsert(query, update, "user");
		System.out.println(upsert.getMatchedCount());       //返回执行的条数
	}
	/**
	 * 删除内嵌文档数据
	 */
	@Ignore
	@Test
	public void deleteNestingMongoDB() {
		Query query = Query.query(Criteria.where("_id").is("11").and("user.name").is("大明"));
		Update update = new Update();
		update.unset("user.$");
		UpdateResult upsert = mongoTemplate.updateFirst(query, update, "user");
		System.out.println(upsert.getMatchedCount());       //返回执行的条数
	}
	/**
	 * 删除文档
	 */
	@Ignore
	@Test
	public void deleteMongoDB() {
		Query query = new Query(Criteria.where("_id").is("1"));
		mongoTemplate.remove(query,User.class,"user");
	}
	/**
	 *  match(筛选文档,并在文档子集上做聚合)
	 */
	@Ignore
	@Test
	public void matchMongoDBTest() {
		// 查询条件
		Criteria criteria = new Criteria();
		criteria.and("grade").is(3);
		MatchOperation matchOperation = Aggregation.match(criteria);
		// 查询结果
		AggregationResults<User> results = mongoTemplate.aggregate(
				Aggregation.newAggregation(User.class, matchOperation),"user",
				User.class);
		System.out.println(results.getMappedResults());
	}
	/**
	 * group(将文档依据字段的不同值进行分组)
	 */
	@Ignore
	@Test
	public void groupMongoDBTest() {
		// 查询条件
		Criteria criteria = new Criteria();
		criteria.and("grade").lt(3);
		// 分组查询数据
		Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(criteria), // 查询条件
				Aggregation.group("grade") // 分组条件
						.first("grade").as("grade")
						.first("name").as("name").first("age").as("age")// 查出多少个数据
		);
		AggregationResults<User> results = mongoTemplate.aggregate(aggregation, "user", User.class);
		List<User> list = results.getMappedResults();
		list.forEach(item -> {
			System.out.println(item);
		});
	}
	/**
	 * sort排序(建议提前排序,可以使用索引)
	 */
	@Ignore
	@Test
	public void sortMongoDBTest() {
		// 查询条件
		Criteria criteria = new Criteria();
		criteria.and("grade").lt(3);
		// 分组查询数据
		Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(criteria),
				Aggregation.sort(new Sort(Sort.Direction.DESC,"age"))  // 排序
//		        .and(new Sort(Sort.Direction.ASC,"grade"))
				);
		AggregationResults<User> results = mongoTemplate.aggregate(aggregation, "user", User.class);
		List<User> list = results.getMappedResults();
		list.forEach(item -> {
			System.out.println(item);
		});
	}
	/**
	 * project(可以重命名字段,可以在结果中移除不需要的字段)
	 */
	@Ignore
	@Test
	public void projectMongoDBTest() {
		// 查询条件
		Criteria criteria = new Criteria();
		criteria.and("grade").lt(3);
		//
		Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(criteria),
				Aggregation.project("_id", "name", "age", "grade")
		);
		AggregationResults<User> results = mongoTemplate.aggregate(aggregation, "user", User.class);
		List<User> list = results.getMappedResults();
		list.forEach(item -> {
			System.out.println(item);
		});
	}
	/**
	 * count(直接查询,用索引反而会慢)表示数量
	 */
	@Ignore
	@Test
	public void countMongoDBTest() {
		// 查询条件
		Criteria criteria = new Criteria();
		criteria.and("grade").lt(3);
		// 分组查询数据
		Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(criteria), // 查询条件
				Aggregation.group("grade") // 分组条件
						.first("grade").as("grade")
						.first("name").as("name").first("age").as("age")// 查出多少个数据
						.count().as("totalUser")
		);
		AggregationResults<UserCount> results = mongoTemplate.aggregate(aggregation, "user", UserCount.class);
		List<UserCount> list = results.getMappedResults();
		list.forEach(item -> {
			System.out.println(item);
		});
	}
	/**
	 * sum(计算数字总和)
	 */
	@Ignore
	@Test
	public void sumMongoDBTest() {
		// 查询条件
		Criteria criteria = new Criteria();
		criteria.and("grade").lt(3);
		// 分组查询数据
		Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(criteria), // 查询条件
				Aggregation.group("grade") // 分组条件
						.first("grade").as("grade")
						.first("name").as("name").first("age").as("age")// 查出多少个数据
						.sum("grade").as("totalUser")
		);
		AggregationResults<UserCount> results = mongoTemplate.aggregate(aggregation, "user", UserCount.class);
		List<UserCount> list = results.getMappedResults();
		list.forEach(item -> {
			System.out.println(item);
		});
	}

	/**
	 * avg平均分
	 */
	@Ignore
	@Test
	public void avgMongoDBTest() {
		// 查询条件
		Criteria criteria = new Criteria();
		criteria.and("grade").lt(3);
		// 分组查询数据
		Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(criteria), // 查询条件
				Aggregation.group("grade") // 分组条件
						.first("grade").as("grade").first("name").as("name").first("age").as("age")// 查出多少个数据
						.avg("grade").as("avgUser"));
		AggregationResults<UserCount> results = mongoTemplate.aggregate(aggregation, "user", UserCount.class);
		List<UserCount> list = results.getMappedResults();
		list.forEach(item -> {
			System.out.println(item);
		});
	}
	/**
	 * limit
	 */
	@Ignore
	@Test
	public void limitMongoDBTest() {
		// 查询条件
		Criteria criteria = new Criteria();
		criteria.and("grade").lt(3);
		// 分组查询数据
		Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(criteria), // 查询条件
				Aggregation.limit(3));
		AggregationResults<User> results = mongoTemplate.aggregate(aggregation, "user", User.class);
		List<User> list = results.getMappedResults();
		list.forEach(item -> {
			System.out.println(item);
		});
	}
	/**
	 * skip(避免使用skip来略过大量的结果,可以利用上次的查询来获取分页结果)
	 */
	@Ignore
	@Test
	public void skipMongoDBTest() {
		// 查询条件
		Criteria criteria = new Criteria();
		criteria.and("grade").lt(3);
		// 分组查询数据
		Aggregation aggregation = Aggregation.newAggregation(Aggregation.match(criteria), // 查询条件
				Aggregation.skip(1L));
		AggregationResults<User> results = mongoTemplate.aggregate(aggregation, "user", User.class);
		List<User> list = results.getMappedResults();
		list.forEach(item -> {
			System.out.println(item);
		});
	}
	
	/**
	 * unwind(可以将数组拆分成子文档)
	 */
	@Ignore
	@Test
	public void unwindMongoDBTest() {
		// 查询条件
		Criteria criteria = new Criteria();
		criteria.and("_id").is(1);
		// 分组查询数据
		Aggregation aggregation = Aggregation.newAggregation( // 查询条件
				Aggregation.match(criteria), 
				Aggregation.unwind("user","arrayIndex",true));
		AggregationResults<Document> results = mongoTemplate.aggregate(aggregation, "user", Document.class);
		List<Document> list = results.getMappedResults();
		list.forEach(item -> {
			System.out.println(item);
		});
	}
	
	/**
	 * mapreduce
	 */
	@Ignore
	@Test
	public void mapReduceMongoDBTest() {
		//执行map reduce操作
        Criteria criteria1=new Criteria("level");
        criteria1.is("A");
        Query query1 = new Query(criteria1);
        MapReduceOptions options = MapReduceOptions.options();
        options.outputCollection("total_score");
        options.outputTypeReduce();
        MapReduceResults<MapReduceResult> reduceResults =
                mongoTemplate.mapReduce(query1,"student",
                        "classpath:map.js",
                        "classpath:reduce.js",
                        options,
                        MapReduceResult.class);
        for(MapReduceResult reduceResult:reduceResults){
            System.out.println("map reduce的结果如下：=========");
            System.out.println("姓名："+reduceResult.getId()+",A的总分："+reduceResult.getValue());
        }
		
		
		
	}
}
