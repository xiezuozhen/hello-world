package com.xzz.mongodb.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapreduce.MapReduceOptions;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xzz.mongodb.entity.MapReduceResult;
import com.xzz.mongodb.entity.Student;
import com.xzz.mongodb.entity.User;


/**
 * @author Administrator
 * @date 2019/02/25
 */
@RestController
@RequestMapping("/map-reduce")
public class TestController {

    @Autowired
    private MongoTemplate mongoTemplate;
    
    @GetMapping(value = "/add")
    public void addTest() {
        List<Student> studentList = new ArrayList<Student>();
        studentList.add(new Student("张三","英语",70,"C"));
        studentList.add(new Student("张三","数学",95,"A"));
        studentList.add(new Student("张三","语文",91,"A"));
        studentList.add(new Student("张三","历史",98,"A"));
        studentList.add(new Student("李四","数学",88,"B"));
        studentList.add(new Student("李四","英语",93,"A"));
        studentList.add(new Student("李四","语文",99,"A"));
        mongoTemplate.insert(studentList, "student");
    
    }
    

    @RequestMapping(value = "/result",method = RequestMethod.GET)
    public void getTest(){
    	//执行map reduce操作
        Criteria criteria1=new Criteria("level");
        criteria1.is("A");
        Query query1 = new Query(criteria1);
        MapReduceOptions options = MapReduceOptions.options();
        options.outputCollection("total_score");
        options.outputTypeReduce();
        MapReduceResults<MapReduceResult> reduceResults =
                mongoTemplate.mapReduce(query1,"student",
                        "classpath:map.js",
                        "classpath:reduce.js",
                        options,
                        MapReduceResult.class);
        for(MapReduceResult reduceResult:reduceResults){
            System.out.println("map reduce的结果如下：=========");
            System.out.println("姓名："+reduceResult.getId()+",A的总分："+reduceResult.getValue());
        }
    }
}
