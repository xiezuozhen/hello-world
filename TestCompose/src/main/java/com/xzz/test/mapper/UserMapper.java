package com.xzz.test.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xzz.test.entity.User;

public interface UserMapper extends BaseMapper<User> {

}
