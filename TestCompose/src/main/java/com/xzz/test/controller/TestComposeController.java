package com.xzz.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xzz.test.entity.User;
import com.xzz.test.mapper.UserMapper;

@RestController
public class TestComposeController {
	@Autowired
    private UserMapper userMapper;
	
	
	@GetMapping("/select")
    public String select() {
        User user = userMapper.selectById(1);
        return "success:{name:"+user.getName()+",age:"+user.getAge()+"}";
    }
	
}
