package com.xzz.test;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.xzz.test.mapper")
public class TestComposeApplication {
	public static void main(String[] args) {
		SpringApplication.run(TestComposeApplication.class, args);
	}
}
