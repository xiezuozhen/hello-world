package com.xzz.etcd.jetcd;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import io.etcd.jetcd.Client;
import io.etcd.jetcd.Election;
import io.etcd.jetcd.KV;
import io.etcd.jetcd.Lease;
import io.etcd.jetcd.Lock;
import io.etcd.jetcd.Watch;

/**
 * jectd 构建与etcd的操作客户端
 * @author luzl
 *
 */
@Component
@PropertySource(value= {"file:./config/jetcd.properties"},ignoreResourceNotFound=true)
public class JetcdOperatorClient {
	@Value("${endpoints:http://192.168.47.210:2379}")
	private String endpoints;
	
	private Client client;
	
	private KV kv;
	
	private Lock lock;
	
	private Lease lease;
	
	private Watch watch;
	
	private Election election;
	
	private final String DOUHAO = ",";
	
	@PostConstruct
	private void init() {
		client=Client.builder().endpoints(endpoints.split(DOUHAO)).build();
		kv=client.getKVClient();
		lock=client.getLockClient();
		lease=client.getLeaseClient();
		watch=client.getWatchClient();
		election=client.getElectionClient();		
	}
	
	@PreDestroy
	private void destroy() {
		election.close();
		kv.close();
		lease.close();
		watch.close();
		lock.close();
		client.close();
	}

	public KV getKv() {
		return kv;
	}

	public void setKv(KV kv) {
		this.kv = kv;
	}

	public Lock getLock() {
		return lock;
	}

	public void setLock(Lock lock) {
		this.lock = lock;
	}

	public Lease getLease() {
		return lease;
	}

	public void setLease(Lease lease) {
		this.lease = lease;
	}

	public Watch getWatch() {
		return watch;
	}

	public void setWatch(Watch watch) {
		this.watch = watch;
	}

	public Election getElection() {
		return election;
	}

	public void setElection(Election election) {
		this.election = election;
	}
}
