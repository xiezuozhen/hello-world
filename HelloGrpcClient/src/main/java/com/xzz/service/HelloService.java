package com.xzz.service;

import com.xzz.api.GreeterGrpc;
import com.xzz.api.HelloReply;
import com.xzz.api.HelloRequest;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

/**
 * @ClassName HelloService
 * @Description
 * @Author xzz
 * @Date 2024/10/17 10:19
 */
@Service
public class HelloService {
    @GrpcClient("ms-zeus")
    private GreeterGrpc.GreeterBlockingStub simpleStub;

    public void testHello(){
        HelloRequest helloRequest = HelloRequest.newBuilder().setName("1").build();
        HelloReply helloReply = simpleStub.sayHello(helloRequest);
        System.out.println(helloReply.getMessage());
    }
}
