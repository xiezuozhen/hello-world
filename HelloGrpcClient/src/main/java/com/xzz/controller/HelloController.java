package com.xzz.controller;

import com.xzz.service.HelloService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;

/**
 * @ClassName HelloController
 * @Description
 * @Author xzz
 * @Date 2024/10/17 10:31
 */
@Controller
public class HelloController {
    @Resource
    private HelloService helloService;

    @GetMapping("hellogrpc")
    public void hello000() {
        helloService.testHello();
    }
}
