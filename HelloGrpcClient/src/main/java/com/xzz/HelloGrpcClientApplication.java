package com.xzz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName HelloGrpcClientApplication
 * @Description
 * @Author xzz
 * @Date 2024/10/17 10:29
 */
@Slf4j
@SpringBootApplication
public class HelloGrpcClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(HelloGrpcClientApplication.class, args);
    }
}
