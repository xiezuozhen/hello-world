package com.xzz.test.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Leave {

    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("age")
    private Integer age;

    @JsonProperty("name")
    private String name;

    @JsonProperty("sex")
    private String sex;

    @JsonProperty("count")
    private Integer count;

    @JsonProperty("status")
    private String status;

}
