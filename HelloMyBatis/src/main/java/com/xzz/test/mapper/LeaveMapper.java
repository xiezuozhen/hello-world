package com.xzz.test.mapper;

import com.xzz.test.domain.Leave;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName LeaveMapper
 * @Description
 * @Author xzz
 * @Date 2025/2/28 15:38
 */
@Mapper
public interface LeaveMapper {
    List<Leave> findAll();
}
