package com.xzz.test.service;

import com.xzz.test.domain.Leave;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName LeaveService
 * @Description
 * @Author xzz
 * @Date 2025/2/28 15:40
 */

public interface LeaveService {
    List<Leave> findAll();
}
