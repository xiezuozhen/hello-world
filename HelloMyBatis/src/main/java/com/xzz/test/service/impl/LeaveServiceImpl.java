package com.xzz.test.service.impl;

import com.xzz.test.domain.Leave;
import com.xzz.test.mapper.LeaveMapper;
import com.xzz.test.service.LeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName LeaveServiceImpl
 * @Description
 * @Author xzz
 * @Date 2025/2/28 15:41
 */
@Service("LEAVE_SERVICE")
public class LeaveServiceImpl implements LeaveService {
    @Autowired
    private LeaveMapper leaveDao;

    @Override
    public List<Leave> findAll() {
        return leaveDao.findAll();
    }

}
