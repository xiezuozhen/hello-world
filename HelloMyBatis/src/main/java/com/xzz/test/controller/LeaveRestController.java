package com.xzz.test.controller;

import com.xzz.test.domain.Leave;
import com.xzz.test.service.LeaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName LeaveRestController
 * @Description
 * @Author xzz
 * @Date 2025/2/28 15:51
 */
@RestController
public class LeaveRestController {
    @Autowired
    private LeaveService leaveService;

    public class ResponseDto {
        Integer code;
        String msg;
        Object result;
        public Integer getCode() {
            return code;
        }
        public void setCode(Integer code) {
            this.code = code;
        }
        public String getMsg() {
            return msg;
        }
        public void setMsg(String msg) {
            this.msg = msg;
        }
        public Object getResult() {
            return result;
        }
        public void setResult(Object result) {
            this.result = result;
        }

    }

    @RequestMapping(value = "/entity/zxasa/leave", method = RequestMethod.GET)
    public ResponseDto findAlls(){
        ResponseDto resp=new ResponseDto();
        try
        {
            List<Leave> lst=leaveService.findAll();
            resp.setCode(200);
            resp.setMsg("success");
            resp.setResult(lst);
        }catch (Exception e)
        {
            resp.setCode(500);
            resp.setMsg(e.getMessage());
            resp.setResult(null);
        }
        return resp;
    }

}
