package com.xzz.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloEsApplication {
	public static void main(String[] args) {
        SpringApplication.run(HelloEsApplication.class, args);
    }
}
