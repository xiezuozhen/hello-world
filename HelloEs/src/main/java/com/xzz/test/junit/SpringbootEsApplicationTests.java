package com.xzz.test.junit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.core.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xzz.test.entity.User;

//https://blog.csdn.net/qq_38366063/article/details/125937004

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)  //使用junit4进行测试
public class SpringbootEsApplicationTests {

    @Autowired
    public RestHighLevelClient restHighLevelClient;

    // 创建索引
    @Test
//    @Ignore
    public void createIndex() throws IOException {
        CreateIndexRequest yida = new CreateIndexRequest("yida");
        CreateIndexResponse createIndexResponse = restHighLevelClient.indices().create(yida, RequestOptions.DEFAULT);
        System.out.println(JSONObject.toJSONString(createIndexResponse));
        restHighLevelClient.close();
    }

    // 测试获取索引，并判断其是否存在
    @Test
    @Ignore
    public void testIndexIsExists() throws IOException {
        GetIndexRequest request = new GetIndexRequest("yida");
        boolean exists = restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
        System.out.println(exists);// 索引是否存在
        restHighLevelClient.close();
    }

    // 删除索引
    @Test
    @Ignore
    public void testDeleteIndex() throws IOException {
        DeleteIndexRequest request = new DeleteIndexRequest("yida");
        AcknowledgedResponse response = restHighLevelClient.indices().delete(request, RequestOptions.DEFAULT);
        System.out.println(response.isAcknowledged());// 是否删除成功
        restHighLevelClient.close();
    }
    
    /**
     * 文档
     * @throws IOException
     */
    // 测试添加文档(先创建一个User实体类，添加fastjson依赖)
    @Test
    @Ignore
    public void testAddDocument() throws IOException {
        // 创建一个User对象
        User yida = new User("yida", 18);
        // 创建请求
        IndexRequest request = new IndexRequest("yida");
        // 制定规则 PUT /yida_index/_doc/1
        request.id("1");// 设置文档ID
        request.timeout(TimeValue.timeValueMillis(1000));// request.timeout("1s")
        // 将我们的数据放入请求中
        request.source(JSON.toJSONString(yida), XContentType.JSON);
        // 客户端发送请求，获取响应的结果
        IndexResponse response = restHighLevelClient.index(request, RequestOptions.DEFAULT);
        System.out.println(response.status());// 获取建立索引的状态信息 CREATED
        System.out.println(response);// 查看返回内容 IndexResponse[index=yida_index,type=_doc,id=1,version=1,result=created,seqNo=0,primaryTerm=1,shards={"total":2,"successful":1,"failed":0}]
    }

    // 获取文档，判断是否存在 get /yida/_doc/1
    @Test
    @Ignore
    public void testDocumentIsExists() throws IOException {
        GetRequest request = new GetRequest("yida", "1");
        // 不获取返回的 _source的上下文了
        request.fetchSourceContext(new FetchSourceContext(false));
        request.storedFields("_none_");
        boolean exists = restHighLevelClient.exists(request, RequestOptions.DEFAULT);
        System.out.println(exists);
        restHighLevelClient.close();
    }



    // 测试更新文档内容
    @Test
    @Ignore
    public void testUpdateDocument() throws IOException {
        UpdateRequest request = new UpdateRequest("yida", "1");
        User user = new User("doudou",11);
        request.doc(JSON.toJSONString(user),XContentType.JSON);
        UpdateResponse response = restHighLevelClient.update(request, RequestOptions.DEFAULT);
        System.out.println(response.status()); // OK
        restHighLevelClient.close();
    }

    // 测试删除文档
    @Test
    @Ignore
    public void testDeleteDocument() throws IOException {
        DeleteRequest request = new DeleteRequest("yida", "1");
        request.timeout("1s");
        DeleteResponse response = restHighLevelClient.delete(request, RequestOptions.DEFAULT);
        System.out.println(response.status());// OK
    }

    // 测试获得文档信息
    @Test
    @Ignore
    public void testGetDocument() throws IOException {
        GetRequest request = new GetRequest("yida","1");
        GetResponse response = restHighLevelClient.get(request, RequestOptions.DEFAULT);
        System.out.println(response.getSourceAsString());// 打印文档内容
        System.out.println(request);// 返回的全部内容和命令是一样的
        restHighLevelClient.close();
    }

    // 查询
 // SearchRequest 搜索请求
 // SearchSourceBuilder 条件构造
 // HighlightBuilder 高亮
 // TermQueryBuilder 精确查询
 // MatchAllQueryBuilder
 // xxxQueryBuilder ...
     @Test
     @Ignore
     public void testSearch() throws IOException {
         // 1.创建查询请求对象
         SearchRequest searchRequest = new SearchRequest("yida");
         // 2.构建搜索条件
         SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
         // (1)查询条件 使用QueryBuilders工具类创建
         // 精确查询
         TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", "张");
         //        // 匹配查询
         //        MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();
         // (2)其他<可有可无>：（可以参考 SearchSourceBuilder 的字段部分）
         // 设置高亮
         HighlightBuilder highlightBuilder = new HighlightBuilder();
         highlightBuilder = highlightBuilder.requireFieldMatch(false).field("*").preTags("<font color='red'>").postTags("</font>");
         searchSourceBuilder.highlighter(highlightBuilder);
         //        // 分页
         //        searchSourceBuilder.from();
         //        searchSourceBuilder.size();
         searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
         // (3)条件投入
         searchSourceBuilder.query(termQueryBuilder);
         // 3.添加条件到请求
         searchRequest.source(searchSourceBuilder);
         // 4.客户端查询请求
         SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
         // 5.查看返回结果
         SearchHits hits = search.getHits();
         System.out.println(JSON.toJSONString(hits));
         System.out.println("=======================");

         for (SearchHit documentFields : hits.getHits()) {
             System.out.println(documentFields.getSourceAsMap());
             // 使用新的字段值（高亮），覆盖旧的字段值
             Map<String, Object> sourceAsMap = documentFields.getSourceAsMap();
             // 高亮字段
             Map<String, HighlightField> highlightFields = documentFields.getHighlightFields();
             HighlightField name = highlightFields.get("name");
             // 替换
             if (name != null){
                 Text[] fragments = name.fragments();
                 StringBuilder new_name = new StringBuilder();
                 for (Text text : fragments) {
                     new_name.append(text);
                 }
                 sourceAsMap.put("name",new_name.toString());
             }
             System.out.println("替换高亮的字段"+JSONObject.toJSON(sourceAsMap));
         }
     }
     
     // 批量插入数据
     @Test
     @Ignore
     public void testBulkAdd() throws IOException {
         BulkRequest bulkRequest = new BulkRequest();
         bulkRequest.timeout("10s");
         ArrayList<User> users = new ArrayList<>();
         users.add(new User("张益达-1",1));
         users.add(new User("张益达-2",2));
         users.add(new User("张益达-3",3));
         users.add(new User("张益达-4",4));
         users.add(new User("张益达-5",5));
         users.add(new User("张益达-6",6));
         // 批量请求处理
         for (int i = 0; i < users.size(); i++) {
             bulkRequest.add(
                     // 这里是数据信息
                     new IndexRequest("yida")
                             .id(""+(i + 1)) // 没有设置id 会自定生成一个随机id
                             .source(JSON.toJSONString(users.get(i)),XContentType.JSON)
             );
         }
         BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
         System.out.println(bulk.status());// ok
     }

     @SuppressWarnings({ "rawtypes", "unchecked" })
	 @Test
     @Ignore
     public void testBulkUpdate() throws IOException {
         BulkRequest bulkRequest = new BulkRequest();
         bulkRequest.timeout("10s");
         ArrayList<User> users = new ArrayList<>();
         users.add(new User("张益达-1-update1",1));
         users.add(new User("张益达-2-update1",2));
         users.add(new User("张益达-3-update",3));
         users.add(new User("张益达-4-update",4));
         users.add(new User("张益达-5-update",5));
         users.add(new User("张益达-6-update",6));
         // 批量请求处理
         for (int i = 0; i < users.size(); i++) {

             Map map = JSON.parseObject(JSONObject.toJSONString(users.get(i)), Map.class);
             bulkRequest.add(
                     new UpdateRequest("yida", "" + (i + 1))
                             .doc(map)
                             // 不存在就插入
                             .upsert(JSON.toJSONString(users.get(i)),XContentType.JSON)
             );
         }
         BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
         System.out.println(bulk.status());// ok
     }

     @Test
     @Ignore
     public void testBulkDelete() throws IOException {
         BulkRequest bulkRequest = new BulkRequest();
         bulkRequest.timeout("10s");
         ArrayList<User> users = new ArrayList<>();
         users.add(new User("张益达-1",1));
         users.add(new User("张益达-2",2));
         users.add(new User("张益达-3",3));
         users.add(new User("张益达-4",4));
         users.add(new User("张益达-5",5));
         users.add(new User("张益达-6",6));
         // 批量请求处理
         for (int i = 0; i < users.size(); i++) {
             bulkRequest.add(
                     // 这里是数据信息
                     new DeleteRequest("yida")
                             .id("" + (i + 1))
             );
         }
         BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
         System.out.println(bulk.status());// ok
     }
}

