package com.xzz.test.activemq;

import com.xzz.test.entity.TopicInfo;
import org.apache.activemq.broker.jmx.BrokerViewMBean;
import org.apache.activemq.broker.jmx.TopicViewMBean;
import org.apache.commons.lang3.StringUtils;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @ClassName ActiveMqTopicTest
 * @Description
 * @Author xzz
 * @Date 2025/3/12 18:14
 */
public class ActiveMqTopicTest {
    public static String virtualTopic = "VirtualTopic.";

    public static String VIRTUAL_TOPIC_TYPE = "VIRTUAL_TOPIC";

    public static String TOPIC_TYPE = "TOPIC";

    ActiveMqBrokerTest brokerService = new ActiveMqBrokerTest();

    public List<TopicInfo> getTopics(String machineName, String url, String usename, String password) {
        List<TopicInfo> topics = new ArrayList<TopicInfo>();
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            ObjectName[] topicsObjectName = mBean.getTopics();
                            for (ObjectName na : topicsObjectName) {
                                TopicInfo topicInfo = getTopicInfoByObjectName(conn, na, machineName);
                                topics.add(topicInfo);
                            }
                            break;
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return topics;
    }


    /**
     *
     * 根据topic名字获取信息
     * @param conn
     *            链接
     * @param na
     *            objectName名字
     * @param machineName
     *            机器名称
     * @return topic详情
     * @date 2022/11/17 17:32:00
     */
    public TopicInfo getTopicInfoByObjectName(MBeanServerConnection conn, ObjectName na, String machineName){
        TopicViewMBean topicBean = (TopicViewMBean) MBeanServerInvocationHandler
                .newProxyInstance(conn, na, TopicViewMBean.class, true);
        TopicInfo topicInfo = new TopicInfo();
        topicInfo.setConsumerCount(topicBean.getConsumerCount());
        topicInfo.setEnqueueCount(topicBean.getEnqueueCount());
        topicInfo.setDequeueCount(topicBean.getDequeueCount());
        topicInfo.setTopicName(topicBean.getName());
        topicInfo.setMachineName(machineName);
        if (topicBean.getName().startsWith(virtualTopic)) {
            topicInfo.setTopicType(VIRTUAL_TOPIC_TYPE);
            topicInfo.setTopicTypeName("虚拟主题");
        } else {
            topicInfo.setTopicType(TOPIC_TYPE);
            topicInfo.setTopicTypeName("主题");
        }
        return topicInfo;
    }

    public boolean addTopic(String topicName, String url, String usename, String password) {
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            mBean.addTopic(topicName);
                            return true;
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println("addTopic exception"+ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return false;
    }


    public boolean removeTopic(String topicName, String url, String usename, String password) {
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            mBean.removeTopic(topicName);
                            return true;
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println("removeTopic exception:{}"+ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return false;
    }


    public List<TopicInfo> getTopicsByQueueName(String machineName, String url, String userName, String password, List<String> names) {
        Map<String, String> collect = names.stream().collect(Collectors.toMap(Function.identity(), Function.identity()));
        List<TopicInfo> topics = new ArrayList<TopicInfo>();
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, userName, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            ObjectName[] topicsObjectName = mBean.getTopics();
                            for (ObjectName na : topicsObjectName) {
                                if (StringUtils.isBlank(collect.get(na.getKeyProperty("destinationName")))){
                                    continue;
                                }
                                TopicInfo topicInfo = getTopicInfoByObjectName(conn, na, machineName);
                                topics.add(topicInfo);
                            }
                            break;
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return topics;
    }




}
