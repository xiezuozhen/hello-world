package com.xzz.test.activemq;

import com.xzz.test.entity.ConnectionInfo;
import org.apache.activemq.broker.jmx.BrokerViewMBean;
import org.apache.activemq.broker.jmx.ConnectionViewMBean;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName ActiveMqConnectionTest
 * @Description
 * @Author xzz
 * @Date 2025/3/12 18:24
 */
public class ActiveMqConnectionTest {
    ActiveMqBrokerTest brokerService = new ActiveMqBrokerTest();


    public List<ConnectionInfo> getConnections(String machineName, String url, String usename, String password) {
        List<ConnectionInfo> connections = new ArrayList<ConnectionInfo>();
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        if (brokerName == null) {
                            BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                            if (mBean != null) {
                                brokerName = mBean.getBrokerName();
                            }
                        }
                        if (brokerName != null) {
                            ObjectName[] arr = brokerService.getConnectionObjectNames(conn, brokerName);
                            for (int i = 0; i < arr.length; i++) {
                                ConnectionViewMBean connectionViewMBean = MBeanServerInvocationHandler
                                        .newProxyInstance(conn, arr[i], ConnectionViewMBean.class, true);
                                if (connectionViewMBean != null) {
                                    ConnectionInfo conInfo = new ConnectionInfo();
                                    conInfo.setConnectionName(arr[i].getKeyProperty("connectionName"));
                                    conInfo.setMachineName(machineName);
                                    conInfo.setRemoteAddress(connectionViewMBean.getRemoteAddress());
                                    conInfo.setConnectorName(arr[i].getKeyProperty("connectorName"));
                                    conInfo.setActive(connectionViewMBean.isActive());
                                    conInfo.setSlow(connectionViewMBean.isSlow());
                                    conInfo.setBlocked(connectionViewMBean.isBlocked());
                                    conInfo.setConnected(connectionViewMBean.isConnected());
                                    conInfo.setClientId(connectionViewMBean.getClientId());
                                    conInfo.setUserName(connectionViewMBean.getUserName());
                                    connections.add(conInfo);
                                }
                            }
                            if (arr != null && 0 < arr.length) {
                                break;
                            }
                        }
                    }
                }

            } catch (Exception ex) {
                System.out.println("run iAqConnectionManager failed"+ex);
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector failed"+ex);
                    }
                }
            }
        }
        return connections;
    }


    public boolean removeConnection(String connectionName, String url, String usename, String password) {
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        if (brokerName == null) {
                            BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                            if (mBean != null) {
                                brokerName = mBean.getBrokerName();
                            }
                        }
                        if (brokerName != null) {
                            ObjectName[] arr = brokerService.getConnectionObjectNames(conn, brokerName);
                            String[] conNameArr = connectionName.split(",");
                            for (String conName : conNameArr) {
                                for (int i = 0; i < arr.length; i++) {
                                    if (conName.equals(arr[i].getKeyProperty("connectionName"))) {
                                        ConnectionViewMBean connectionViewMBean = MBeanServerInvocationHandler
                                                .newProxyInstance(conn, arr[i], ConnectionViewMBean.class, true);
                                        if (connectionViewMBean != null) {
                                            connectionViewMBean.stop();
                                        }
                                        break;
                                    }
                                }
                            }
                            return true;
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println("removeConnection exception:{}"+ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return false;
    }
}
