package com.xzz.test.activemq;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xzz.test.activemq.ActiveMqBrokerTest;
import com.xzz.test.entity.MsgInfo;
import com.xzz.test.entity.QueueInfo;
import org.apache.activemq.broker.jmx.BrokerViewMBean;
import org.apache.activemq.broker.jmx.QueueViewMBean;
import org.apache.commons.lang3.StringUtils;

import javax.jms.Destination;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.remote.JMXConnector;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @ClassName ActiveMqQueueTest
 * @Description
 * @Author xzz
 * @Date 2025/3/12 17:42
 */
public class ActiveMqQueueTest {
    ActiveMqBrokerTest brokerService = new ActiveMqBrokerTest();

    public static String QUEUE_TYPE = "QUEUE";


    public List<QueueInfo> getQueues(String machineName, String url, String usename, String password) {
        List<QueueInfo> queues = new ArrayList<QueueInfo>();
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            ObjectName[] topicsObjectName = mBean.getQueues();
                            for (ObjectName na : topicsObjectName) {
                                // 获取订阅模式的队列
                                QueueInfo queueInfo = getQueueInfoByObjectName(conn, na, machineName);
                                queues.add(queueInfo);
                            }
                            break;
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return queues;
    }


    public List<QueueInfo> getQueuesByQueueName(String machineName, String url, String useName, String password,
                                                List<String> names) {
        Map<String, String> collect = names.stream()
                .collect(Collectors.toMap(Function.identity(), Function.identity()));
        List<QueueInfo> queues = new ArrayList<QueueInfo>();
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, useName, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            ObjectName[] topicsObjectName = mBean.getQueues();
                            for (ObjectName na : topicsObjectName) {
                                if (StringUtils.isBlank(collect.get(na.getKeyProperty("destinationName")))) {
                                    continue;
                                }
                                QueueInfo queueInfo = getQueueInfoByObjectName(conn, na, machineName);
                                queues.add(queueInfo);
                            }
                            break;
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return queues;
    }
    /**
     *
     * 获取队列信息
     *
     * @param conn
     *            链接
     * @param na
     *            队列objectName
     * @param machineName
     *            机器名称
     * @return 生成队列信息
     */
    public QueueInfo getQueueInfoByObjectName(MBeanServerConnection conn, ObjectName na, String machineName) {
        // 获取订阅模式的队列
        QueueViewMBean queueBean = (QueueViewMBean) MBeanServerInvocationHandler.newProxyInstance(conn, na,
                QueueViewMBean.class, true);
        QueueInfo queueInfo = new QueueInfo();
        queueInfo.setQueueName(queueBean.getName());
        queueInfo.setConsumerCount(queueBean.getConsumerCount());
        queueInfo.setDequeueCount(queueBean.getDequeueCount());
        queueInfo.setEnqueueCount(queueBean.getEnqueueCount());
        queueInfo.setQueueSize(queueBean.getQueueSize());
        queueInfo.setStoreMessageSize(queueBean.getStoreMessageSize());
        queueInfo.setMachineName(machineName);
        queueInfo.setType(QUEUE_TYPE);
        queueInfo.setTypeName("队列");
        return queueInfo;
    }


    public boolean addQueue(String queueName, String url, String usename, String password) {
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            mBean.addQueue(queueName);
                            return true;
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println("addQueue exception:{}"+ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return false;
    }

    /**
     *
     * 发现接口queryQueues可以查询单个队列信息 暂时没用到 filter参数：empty - return only empty queues
     * (queueSize = 0) * nonEmpty - return only non-empty queues queueSize != 0)
     * * noConsumer - return only destinations that doesn't have consumers *
     * nonAdvisory - return only non-Advisory topics
     *
     * @param mBean
     *            mBean实例
     * @param conn
     *            链接
     * @param queueName
     *            队列名字
     */
    public void getQueueInfoByQueueName(BrokerViewMBean mBean, MBeanServerConnection conn, String queueName) {

        HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("name", queueName);

        stringStringHashMap.put("filter", "");
        stringStringHashMap.put("sortColumn", "queueSize");
        stringStringHashMap.put("sortOrder", "desc");
        try {
            String param = JSON.toJSONString(stringStringHashMap);
            String info = mBean.queryQueues(param, 1, 1);
            HashMap<String, String> map = JSONObject.parseObject(info, HashMap.class);

        } catch (Exception e) {

        }
    }


    public boolean removeQueue(String queueName, String url, String usename, String password) {
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            mBean.removeQueue(queueName);
                            return true;
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println("removeQueue exception:{}"+ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return false;
    }


    public List<MsgInfo> getQueueMsgs(String queueName, String url, String usename, String password) {
        List<MsgInfo> msgs = new ArrayList<MsgInfo>();
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            CompositeData[] comData = mBean.browseQueue(queueName);
                            Long msgId = 1L;
                            for (CompositeData data : comData) {
                                MsgInfo msgInfo = new MsgInfo();
                                CompositeDataSupport compositeDataSupport = (CompositeDataSupport) data;
                                msgInfo.setMsgId(msgId);
                                msgId = msgId + 1;
                                msgInfo.setMessageId((String) compositeDataSupport.get("JMSMessageID"));
                                msgInfo.setCorrelationId((String) compositeDataSupport.get("JMSCorrelationID"));
                                msgInfo.setDeliveryMode((String) compositeDataSupport.get("JMSDeliveryMode"));
                                msgInfo.setPriority((int) compositeDataSupport.get("JMSPriority"));
                                msgInfo.setRedelivered((boolean) compositeDataSupport.get("JMSRedelivered"));
                                msgInfo.setReplyTo((Destination) compositeDataSupport.get("JMSReplyTo"));
                                msgInfo.setTimestamp((Date) compositeDataSupport.get("JMSTimestamp"));
                                msgInfo.setType((String) compositeDataSupport.get("JMSType"));
                                msgs.add(msgInfo);
                            }
                            break;
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println("getQueueMsg exception:{}"+ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return msgs;
    }


    public List<MsgInfo> getHavePropertiesTextQueueMsgs(String queueName, String url, String usename,
                                                        String password) {
        List<MsgInfo> msgs = new ArrayList<MsgInfo>();
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            CompositeData[] comData = mBean.browseQueue(queueName);
                            Long msgId = 1L;
                            for (CompositeData data : comData) {
                                MsgInfo msgInfo = new MsgInfo();
                                CompositeDataSupport compositeDataSupport = (CompositeDataSupport) data;
                                msgInfo.setMsgId(msgId);
                                msgId = msgId + 1;
                                msgInfo.setMessageId((String) compositeDataSupport.get("JMSMessageID"));
                                msgInfo.setCorrelationId((String) compositeDataSupport.get("JMSCorrelationID"));
                                msgInfo.setDeliveryMode((String) compositeDataSupport.get("JMSDeliveryMode"));
                                msgInfo.setPriority((int) compositeDataSupport.get("JMSPriority"));
                                msgInfo.setRedelivered((boolean) compositeDataSupport.get("JMSRedelivered"));
                                msgInfo.setReplyTo((Destination) compositeDataSupport.get("JMSReplyTo"));
                                msgInfo.setTimestamp((Date) compositeDataSupport.get("JMSTimestamp"));
                                msgInfo.setType((String) compositeDataSupport.get("JMSType"));
                                if (compositeDataSupport.containsKey("PropertiesText")) {
                                    msgInfo.setPropertiesText((String) compositeDataSupport.get("PropertiesText"));
                                }
                                msgs.add(msgInfo);
                            }
                            break;
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println("getQueueMsg exception:{}"+ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return msgs;
    }



    public boolean removeQueueMsg(String queueName, String messageId, String url, String usename,
                                  String password) {
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            for (ObjectName na : mBean.getQueues()) {
                                // 获取订阅模式的队列
                                QueueViewMBean queueBean = (QueueViewMBean) MBeanServerInvocationHandler
                                        .newProxyInstance(conn, na, QueueViewMBean.class, true);
                                if (queueName.equals(queueBean.getName())) {
                                    if (messageId != null && messageId != "") {
                                        String[] msgIdArr = messageId.split(",");
                                        for (String msgId : msgIdArr) {
                                            try {
                                                queueBean.removeMessage(msgId);
                                            }catch (Exception ex) {
                                                System.out.println("removeQueueMsg exception:{}"+ex.getMessage());
                                            }
                                        }
                                    } else {
                                        queueBean.purge();
                                    }
                                    return true;
                                }
                            }

                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println("removeQueueMsg exception:{}"+ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return false;
    }

    public boolean retryQueueMsg(String queueName, String messageId, String url, String usename, String password) {
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            for (ObjectName na : mBean.getQueues()) {
                                // 获取订阅模式的队列
                                QueueViewMBean queueBean = (QueueViewMBean) MBeanServerInvocationHandler
                                        .newProxyInstance(conn, na, QueueViewMBean.class, true);
                                if (queueName.equals(queueBean.getName())) {
                                    if (messageId != null && messageId != "") {
                                        String[] msgIdArr = messageId.split(",");
                                        for (String msgId : msgIdArr) {
                                            try {
                                                queueBean.retryMessage(msgId);
                                            }catch (Exception ex) {
                                                System.out.println("retryQueueMsg exception:{}"+ex.getMessage());
                                            }
                                        }
                                    }
                                    return true;
                                }
                            }

                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println("retryQueueMsg exception:{}"+ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return false;
    }


    public MsgInfo getQueueMsg(String queueName, String messageId, String url, String usename, String password) {
        MsgInfo msgInfo = new MsgInfo();
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            for (ObjectName na : mBean.getQueues()) {
                                // 获取订阅模式的队列
                                QueueViewMBean queueBean = (QueueViewMBean) MBeanServerInvocationHandler
                                        .newProxyInstance(conn, na, QueueViewMBean.class, true);
                                if (queueName.equals(queueBean.getName())) {
                                    CompositeDataSupport compositeDataSupport = (CompositeDataSupport) queueBean
                                            .getMessage(messageId);
                                    msgInfo.setObjectName(queueName);
                                    msgInfo.setMessageId((String) compositeDataSupport.get("JMSMessageID"));
                                    msgInfo.setCorrelationId(
                                            (String) compositeDataSupport.get("JMSCorrelationID"));
                                    msgInfo.setDeliveryMode((String) compositeDataSupport.get("JMSDeliveryMode"));
                                    msgInfo.setPriority((int) compositeDataSupport.get("JMSPriority"));
                                    msgInfo.setRedelivered((boolean) compositeDataSupport.get("JMSRedelivered"));
                                    msgInfo.setReplyTo((Destination) compositeDataSupport.get("JMSReplyTo"));
                                    msgInfo.setTimestamp((Date) compositeDataSupport.get("JMSTimestamp"));
                                    msgInfo.setType((String) compositeDataSupport.get("JMSType"));
                                    msgInfo.setExpiration((long) compositeDataSupport.get("JMSExpiration"));
                                    msgInfo.setGroupId((String) compositeDataSupport.get("JMSXGroupID"));
                                    msgInfo.setGroupSeq((int) compositeDataSupport.get("JMSXGroupSeq"));
                                    if (compositeDataSupport.containsKey("Text")) {
                                        msgInfo.setContent((String) compositeDataSupport.get("Text"));
                                    } else if (compositeDataSupport.containsKey("ContentMap")) {
                                        msgInfo.setContent((String) compositeDataSupport.get("ContentMap"));
                                    }
                                    if (compositeDataSupport.containsKey("JMSXDeliveryCount")) {
                                        msgInfo.setDeliveryCount(
                                                (int) compositeDataSupport.get("JMSXDeliveryCount"));
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println("getQueueMsg exception:{}"+ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return msgInfo;
    }


    public boolean copyQueueMsg(String queueName, String toQueueName, String selector, String url, String usename,
                                String password) {
        JMXConnector connector = null;
        List<String> urlList = new ArrayList(Arrays.asList(url.split(",")));
        for (String urlAddress : urlList) {
            try {
                String serviceUrl = brokerService.getServiceURL(urlAddress);
                String brokerName = brokerService.getBrokerName(urlAddress);
                connector = brokerService.getJMXConnector(serviceUrl, usename, password);
                if (connector != null) {
                    MBeanServerConnection conn = brokerService.getMBeanServerConnection(connector);
                    if (conn != null) {
                        BrokerViewMBean mBean = brokerService.getBrokerViewMBean(conn, brokerName);
                        if (mBean != null) {
                            for (ObjectName na : mBean.getQueues()) {
                                // 获取订阅模式的队列
                                QueueViewMBean queueBean = (QueueViewMBean) MBeanServerInvocationHandler
                                        .newProxyInstance(conn, na, QueueViewMBean.class, true);
                                if (queueName.equals(queueBean.getName())) {
                                    queueBean.copyMatchingMessagesTo(selector, toQueueName);
                                    return true;
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                System.out.println("copyQueueMsg exception:{}"+ex.getMessage());
            } finally {
                if (connector != null) {
                    try {
                        connector.close();
                    } catch (IOException ex) {
                        System.out.println("close jmx connector:{}"+ex.getMessage());
                    }
                }
            }
        }
        return false;
    }






}
