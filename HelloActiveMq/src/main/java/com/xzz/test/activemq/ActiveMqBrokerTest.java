package com.xzz.test.activemq;

import org.apache.activemq.broker.jmx.BrokerViewMBean;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName ActiveMqTest
 * @Description
 * @Author xzz
 * @Date 2025/3/12 17:34
 */
public class ActiveMqBrokerTest {


    public String getServiceURL(String url) {
        int index = url.lastIndexOf("/");
        if (index > -1) {
            return url.substring(0, index);
        }
        return url;
    }


    public String getBrokerName(String url) {
        int index = url.lastIndexOf("/");
        if (index > -1) {
            return url.substring(index + 1);
        }
        return null;
    }


    public JMXConnector getJMXConnector(String url, String usename, String password) {
        JMXConnector connector = null;
        try {
            System.out.println("jmx service url:"+url);
            StringBuffer sbf = new StringBuffer("service:jmx:rmi:///jndi/rmi://").append(url).append("/jmxrmi");
            JMXServiceURL urls = new JMXServiceURL(sbf.toString());
            Map<String, Object> credentials = new HashMap<String, Object>();
            credentials.put(JMXConnector.CREDENTIALS, new String[] { usename, password });
            connector = JMXConnectorFactory.connect(urls, credentials);
            connector.connect();
        } catch (Exception ex) {
            System.out.println("activemq jmx connection exception:"+ex.getMessage());
            if (connector != null) {
                try {
                    connector.close();
                } catch (IOException iex) {
                    System.out.println("close jmx connector:{} "+iex.getMessage());
                }
            }
        }
        return connector;
    }

    public MBeanServerConnection getMBeanServerConnection(JMXConnector connector) {
        MBeanServerConnection conn = null;
        try {
            conn = connector.getMBeanServerConnection();
        } catch (Exception ex) {
            System.out.println("activemq server connection exception:{}"+ex.getMessage());
        }
        return conn;
    }

    public BrokerViewMBean getBrokerViewMBean(MBeanServerConnection conn, String brokerName) {
        BrokerViewMBean mBean = null;
        // 这里brokerName的b要小些，大写会报错
        try {
            ObjectName brokerObjName = null;
            // 配置了名称
            if (brokerName != null) {
                StringBuffer sbf = new StringBuffer("org.apache.activemq:brokerName=").append(brokerName)
                        .append(",type=Broker");
                brokerObjName = new ObjectName(sbf.toString());
            } else {
                // 没配置名称
                ObjectName name = new ObjectName("org.apache.activemq:type=Broker,brokerName=*");
                Set<ObjectName> brokers = conn.queryNames(name, null);
                for (ObjectName objectName : brokers) {
                    brokerObjName = objectName;
                    break;
                }
            }
            if (brokerObjName == null) {
                System.out.println("broker ObjectName is null");
            } else {
                mBean = (BrokerViewMBean) MBeanServerInvocationHandler.newProxyInstance(conn, brokerObjName,
                        BrokerViewMBean.class, true);
            }
        } catch (Exception ex) {
            System.out.println("activemq get broker exception:{}"+ex.getMessage());
        }
        return mBean;
    }

    public ObjectName[] getConnectionObjectNames(MBeanServerConnection conn, String brokerName) {
        ObjectName[] arr = new ObjectName[0];
        try {
            StringBuffer sbf = new StringBuffer("org.apache.activemq:type=Broker,brokerName=").append(brokerName)
                    .append(",connector=clientConnectors,connectionViewType=clientId,*");
            ObjectName oName = new ObjectName(sbf.toString());
            Set<ObjectName> queryResult = conn.queryNames(oName, null);

            arr = queryResult.toArray(new ObjectName[queryResult.size()]);
        } catch (Exception ex) {
            System.out.println("activemq get ConnectionObjectNames exception:{}"+ex.getMessage());
        }
        return arr;
    }

    public static void main(String[] args) {
        ActiveMqBrokerTest a = new ActiveMqBrokerTest();
//        a.getJMXConnector("192.168.47.250:11099", "admin", "admin");
        a.getJMXConnector("192.168.47.250:11099", "controlRole", "abcd1234");
    }


}
