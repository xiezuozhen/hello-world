package com.xzz.test.activemq;

import com.xzz.test.entity.TopicInfo;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @ClassName ActiveMqMessageTest
 * @Description
 * @Author xzz
 * @Date 2025/3/12 18:06
 */
public class ActiveMqMessageTest {
    ActiveMqTopicTest topicService = new ActiveMqTopicTest();

//    public PageR<MessageManageListResp> listMessageManage(MessageManageListReq messageManageListReq) {
//        List<MessageManageListResp> messageManageList = new ArrayList<>();
//        //根据查询条件构建sql语句
//        LambdaQueryWrapper<ReleaseMessageApi> wrapper = new LambdaQueryWrapper<ReleaseMessageApi>()
//                .like(!Objects.isNull(messageManageListReq.getTopicName()), ReleaseMessageApi::getTopicName, messageManageListReq.getTopicName())
//                .eq(ReleaseMessageApi :: getDeploymentStatus, 1)
//                .eq(ReleaseMessageApi::getDelFlag,0)
//                .orderByDesc(ReleaseMessageApi :: getCreateTime);
//        //分页
//        Page<ReleaseMessageApi> page = releaseMessageApiService.page(new Page<>(messageManageListReq.getPageIndex(),messageManageListReq.getPageSize()),wrapper);
//        List<ReleaseMessageApi> list = page.getRecords().stream().collect(Collectors.toList());
//        Map<String, ReleaseMessageApi> map =  list.stream().collect(Collectors.toMap(a -> {
//            return a.getMessageMiddlewareAddress()+a.getMessageMiddlewareName()+a.getMessageMiddlewarePwd();
//        }, Function.identity(), (n1, n2) -> n1));
//        Map<String, List<TopicInfo>> mapTopic = new HashMap<>();
//        for (String key : map.keySet()) {
//            ReleaseMessageApi r =  map.get(key);
//            List<TopicInfo> listT = getTopicInfoList(r.getMessageMiddlewareAddress(), r.getMessageMiddlewareName(), r.getMessageMiddlewarePwd());
//            mapTopic.put(key, listT);
//        }
//        messageManageList = page.getRecords().stream().map(a -> {
//            MessageManageListResp messageManageListResp = new MessageManageListResp();
//            messageManageListResp.setTopicName(a.getTopicName());
//            messageManageListResp.setTopicType(MessageTypeEnum.topic.getCode());
//            messageManageListResp.setMessageMiddlewareAddress(a.getMessageMiddlewareAddress());
//            messageManageListResp.setMessageMiddlewareName(a.getMessageMiddlewareName());
//            messageManageListResp.setMessageMiddlewarePwd(a.getMessageMiddlewarePwd());
//            List<TopicInfo> listT = mapTopic.get(a.getMessageMiddlewareAddress()+a.getMessageMiddlewareName()+a.getMessageMiddlewarePwd());
//            TopicInfo topic = listT.stream().filter(b -> b.getTopicName().equals(a.getTopicName())).findFirst().orElse(new TopicInfo());
//            messageManageListResp.setConsumerCount((int) topic.getConsumerCount());
//            messageManageListResp.setEnqueueCount((int) topic.getEnqueueCount());
//            messageManageListResp.setDequeueCount((int) topic.getDequeueCount());
//            return messageManageListResp;
//        }).collect(Collectors.toList());
//        PageR<MessageManageListResp> messageManageListPage = new PageR<>(messageManageList, (int) page.getTotal(),messageManageListReq.getPageIndex(),messageManageListReq.getPageSize());
//        return messageManageListPage;
//    }

    private List<TopicInfo> getTopicInfoList(String messageMiddlewareAddress, String messageMiddlewareName, String messageMiddlewarePwd){
        return topicService.getTopics("activemq",
                messageMiddlewareAddress + ":" + "11099", messageMiddlewareName,
                messageMiddlewarePwd);
    }


    private TopicInfo getTopicInfo(String messageMiddlewareAddress, String messageMiddlewareName, String messageMiddlewarePwd, String topic){
        List<TopicInfo> topicList = topicService.getTopics("activemq",
                messageMiddlewareAddress + ":" + "11099", messageMiddlewareName,
                messageMiddlewarePwd);
        TopicInfo topicInfo = topicList.stream().filter(a -> a.getTopicName().equals(topic)).findFirst().orElse(new TopicInfo());
        return topicInfo;
    }
}
