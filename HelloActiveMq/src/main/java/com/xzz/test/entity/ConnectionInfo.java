package com.xzz.test.entity;

/**
 * @ClassName ConnectionInfo
 * @Description
 * @Author xzz
 * @Date 2025/3/12 18:09
 */
public class ConnectionInfo {


    public String connectionName;
    public String machineName;

    public String remoteAddress;
    /**
     * 连接类型
     */
    public String connectorName;

    public boolean slow;
    public boolean blocked;
    public boolean connected;
    public boolean active;

    protected String clientId;
    protected String userName;

    public String activeStr;
    public String slowStr;

    public String getConnectionName() {
        return connectionName;
    }

    public void setConnectionName(String connectionName) {
        this.connectionName = connectionName;
    }

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public String getConnectorName() {
        return connectorName;
    }

    public void setConnectorName(String connectorName) {
        this.connectorName = connectorName;
    }

    public boolean isSlow() {
        return slow;
    }

    public void setSlow(boolean slow) {
        this.slow = slow;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getActiveStr() {
        return activeStr;
    }

    public void setActiveStr(String activeStr) {
        this.activeStr = activeStr;
    }

    public String getSlowStr() {
        return slowStr;
    }

    public void setSlowStr(String slowStr) {
        this.slowStr = slowStr;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "ConnectionInfo [connectionName=" + connectionName + ", machineName=" + machineName
                + ", remoteAddress=" + remoteAddress + ", connectorName=" + connectorName + ", slow=" + slow
                + ", blocked=" + blocked + ", connected=" + connected + ", active=" + active + "]";
    }

}
