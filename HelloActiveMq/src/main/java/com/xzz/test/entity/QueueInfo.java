package com.xzz.test.entity;

/**
 * @ClassName QueueInfo
 * @Description 
 * @Author xzz
 * @Date 2025/3/12 17:45
 */
public class QueueInfo {
    public String queueName;
    public String machineName;

    public String type;
    public String typeName;

    public long consumerCount;
    public long dequeueCount;
    public long enqueueCount;
    public long queueSize;
    public long storeMessageSize;

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public long getConsumerCount() {
        return consumerCount;
    }

    public void setConsumerCount(long consumerCount) {
        this.consumerCount = consumerCount;
    }

    public long getDequeueCount() {
        return dequeueCount;
    }

    public void setDequeueCount(long dequeueCount) {
        this.dequeueCount = dequeueCount;
    }

    public long getEnqueueCount() {
        return enqueueCount;
    }

    public void setEnqueueCount(long enqueueCount) {
        this.enqueueCount = enqueueCount;
    }

    public long getQueueSize() {
        return queueSize;
    }

    public void setQueueSize(long queueSize) {
        this.queueSize = queueSize;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public long getStoreMessageSize() {
        return storeMessageSize;
    }

    public void setStoreMessageSize(long storeMessageSize) {
        this.storeMessageSize = storeMessageSize;
    }

    @Override
    public String toString() {
        return "QueueInfo [queueName=" + queueName + ", machineName=" + machineName + "]";
    }
}
