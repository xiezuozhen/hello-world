package com.xzz.test.entity;

import javax.jms.Destination;
import java.util.Date;

/**
 * @ClassName MsgInfo
 * @Description TODO
 * @Author xzz
 * @Date 2025/3/12 18:01
 */
public class MsgInfo {
    public Long msgId;
    /**
     * 队列/主题名称
     */
    public String objectName;
    public String messageId;
    public String correlationId;
    public String deliveryMode;
    public int priority;
    public boolean redelivered;
    public Destination replyTo;
    public Date timestamp;
    public String type;

    public String groupId;
    public int groupSeq;
    public int deliveryCount;
    public String content;

    public String propertiesText;

    public String getPropertiesText() {
        return propertiesText;
    }

    public void setPropertiesText(String propertiesText) {
        this.propertiesText = propertiesText;
    }

    /**
     * 到期
     */
    public long expiration;

    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(String deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isRedelivered() {
        return redelivered;
    }

    public void setRedelivered(boolean redelivered) {
        this.redelivered = redelivered;
    }

    public Destination getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(Destination replyTo) {
        this.replyTo = replyTo;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public long getExpiration() {
        return expiration;
    }

    public void setExpiration(long expiration) {
        this.expiration = expiration;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public int getGroupSeq() {
        return groupSeq;
    }

    public void setGroupSeq(int groupSeq) {
        this.groupSeq = groupSeq;
    }

    public int getDeliveryCount() {
        return deliveryCount;
    }

    public void setDeliveryCount(int deliveryCount) {
        this.deliveryCount = deliveryCount;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "MsgInfo [messageId=" + messageId + ", correlationId=" + correlationId + ", deliveryMode="
                + deliveryMode + ", priority=" + priority + ", redelivered=" + redelivered + ", replyTo=" + replyTo
                + ", timestamp=" + timestamp + ", type=" + type + "]";
    }
}
