package com.xzz.test.entity;

/**
 * @ClassName TopicInfo
 * @Description
 * @Author xzz
 * @Date 2025/3/12 18:10
 */
public class TopicInfo {
    public String topicName;
    public String topicType;
    public String topicTypeName;
    public String machineName;

    public long consumerCount;
    public long dequeueCount;
    public long enqueueCount;

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getTopicType() {
        return topicType;
    }

    public void setTopicType(String topicType) {
        this.topicType = topicType;
    }

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public String getTopicTypeName() {
        return topicTypeName;
    }

    public void setTopicTypeName(String topicTypeName) {
        this.topicTypeName = topicTypeName;
    }

    public long getConsumerCount() {
        return consumerCount;
    }

    public void setConsumerCount(long consumerCount) {
        this.consumerCount = consumerCount;
    }

    public long getDequeueCount() {
        return dequeueCount;
    }

    public void setDequeueCount(long dequeueCount) {
        this.dequeueCount = dequeueCount;
    }

    public long getEnqueueCount() {
        return enqueueCount;
    }

    public void setEnqueueCount(long enqueueCount) {
        this.enqueueCount = enqueueCount;
    }

    @Override
    public String toString() {
        return "TopicInfo [topicName=" + topicName + ", topicType=" + topicType + ", topicTypeName="
                + topicTypeName + ", machineName=" + machineName + "]";
    }
}
