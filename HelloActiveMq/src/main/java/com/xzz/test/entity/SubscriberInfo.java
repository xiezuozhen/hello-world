package com.xzz.test.entity;

/**
 * @ClassName SubscriberInfo
 * @Description TODO
 * @Author xzz
 * @Date 2025/3/12 18:09
 */
public class SubscriberInfo {
    public String topicName;
    public String topicType;
    public String topicTypeName;
    public String machineName;

    public String clientId;
    public String subscriberName;
    public String selector;

    public boolean isDurable;
    public boolean isActive;

    public int pendingQueueSize;
    public int dispatchedQueueSize;
    public long dispatchedCounter;
    public long enqueueCounter;
    public long dequeueCounter;

    /**
     * 主题名称查询条件是否要相等
     */
    public boolean topicNameEnFlag = false;

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getTopicType() {
        return topicType;
    }

    public void setTopicType(String topicType) {
        this.topicType = topicType;
    }

    public String getTopicTypeName() {
        return topicTypeName;
    }

    public void setTopicTypeName(String topicTypeName) {
        this.topicTypeName = topicTypeName;
    }

    public String getMachineName() {
        return machineName;
    }

    public void setMachineName(String machineName) {
        this.machineName = machineName;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSubscriberName() {
        return subscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        this.subscriberName = subscriberName;
    }

    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }

    public boolean isDurable() {
        return isDurable;
    }

    public void setDurable(boolean isDurable) {
        this.isDurable = isDurable;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public int getPendingQueueSize() {
        return pendingQueueSize;
    }

    public void setPendingQueueSize(int pendingQueueSize) {
        this.pendingQueueSize = pendingQueueSize;
    }

    public int getDispatchedQueueSize() {
        return dispatchedQueueSize;
    }

    public void setDispatchedQueueSize(int dispatchedQueueSize) {
        this.dispatchedQueueSize = dispatchedQueueSize;
    }

    public long getDispatchedCounter() {
        return dispatchedCounter;
    }

    public void setDispatchedCounter(long dispatchedCounter) {
        this.dispatchedCounter = dispatchedCounter;
    }

    public long getEnqueueCounter() {
        return enqueueCounter;
    }

    public void setEnqueueCounter(long enqueueCounter) {
        this.enqueueCounter = enqueueCounter;
    }

    public long getDequeueCounter() {
        return dequeueCounter;
    }

    public void setDequeueCounter(long dequeueCounter) {
        this.dequeueCounter = dequeueCounter;
    }

    public boolean isTopicNameEnFlag() {
        return topicNameEnFlag;
    }

    public void setTopicNameEnFlag(boolean topicNameEnFlag) {
        this.topicNameEnFlag = topicNameEnFlag;
    }

    @Override
    public String toString() {
        return "SubscriberInfo [topicName=" + topicName + ", topicType=" + topicType + ", topicTypeName="
                + topicTypeName + ", machineName=" + machineName + ", clientId=" + clientId + ", subscriberName="
                + subscriberName + ", selector=" + selector + ", isDurable=" + isDurable + ", isActive=" + isActive
                + ", pendingQueueSize=" + pendingQueueSize + ", dispatchedQueueSize=" + dispatchedQueueSize
                + ", dispatchedCounter=" + dispatchedCounter + ", enqueueCounter=" + enqueueCounter
                + ", dequeueCounter=" + dequeueCounter + "]";
    }
}
