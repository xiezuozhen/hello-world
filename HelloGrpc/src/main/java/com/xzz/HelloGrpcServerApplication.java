package com.xzz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName HelloGrpcServerApplication
 * @Description
 * @Author xzz
 * @Date 2024/10/17 10:27
 */
@Slf4j
@SpringBootApplication
public class HelloGrpcServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(HelloGrpcServerApplication.class, args);
    }
}
