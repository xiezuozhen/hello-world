package com.xzz.service;

import com.xzz.api.GreeterGrpc;
import com.xzz.api.HelloReply;
import com.xzz.api.HelloRequest;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;

/**
 * @ClassName HelloGrpcImpl
 * @Description
 * @Author xzz
 * @Date 2024/10/17 10:10
 */
@GrpcService
public class HelloGrpcImpl extends GreeterGrpc.GreeterImplBase {
    @Override
    public void sayHello(HelloRequest request, StreamObserver<HelloReply> responseObserver) {
        System.out.println("hello");
        responseObserver.onNext(HelloReply.newBuilder().setMessage("hello").build());
        responseObserver.onCompleted();
    }
}
